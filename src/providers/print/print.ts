import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';

import { Print } from "../../interfaces/print.interface";

@Injectable()
export class PrintProvider {

  constructor(private btSerial:BluetoothSerial,
              private alertCtrl:AlertController ) {
  }

  searchBt()
  {
    return this.btSerial.list();
  }

  connectBT(address)
  {
    return this.btSerial.connect(address);

  }

  Print(address, print: Print)
  {
    var printData = '';
    var template = '^XA'
      + '^MMT'
      + '^PW440'
      + '^LL0349'
      + '^LS0'
      + '^FT290,151^BQN,2,5'
      + '^FH\^FDMA,' + print.text1 + '^FS'
      + '^FT221,29^A0R,51,50^FH\^FD' + print.text1 + '^FS'
      + '^FO196,29^GB0,286,4^FS'
      + '^FT275,307^A0N,30,25^FH\^FD' + print.date + '^FS'
      + '^BY2,3,45^FT12,29^BCR,,Y,Y'
      + '^FD>:' + print.text2 + '^FS'
      + '^PQ1,0,1,Y'
      + '^XZ'

      for( var i = 0; i < print.quantity; i++ ){
        printData = printData + template;
      }

      let conexionBt = this.connectBT( address ).subscribe( data => {

      this.btSerial.write(printData).then(dataz=>{
        /*
        let alert = this.alertCtrl.create({
          title:"Imprimiendo!",
          buttons:['Dismiss']
        });
        alert.present();
        */
        conexionBt.unsubscribe();

      },errx=>{
        console.log("Ocurrió un error al imprimir",errx);
        let mno=this.alertCtrl.create({
          title:"¡Ocurrió un error al imprimir!",
          buttons:['Dismiss']
        });
        mno.present();
      });

      },err=>{

        let mno=this.alertCtrl.create({
          title:"¡Ocurrió un error al conectarse con impresora!",
          buttons:['Dismiss']
        });
        mno.present();

      });

  }

}
