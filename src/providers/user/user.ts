import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/share'

import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Api } from '../api/api';
import {TranslateService} from "@ngx-translate/core";
import {ToastController} from "ionic-angular";
import {Observable} from "rxjs/Observable";

/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }Ø
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class User {
  _user: any;
  session: any;
  private errorString: string;

  constructor(public api: Api, private storage: Storage, public toastCtrl: ToastController,
              public translateService: TranslateService) { }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any) {
    var headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    let seq = this.api.post('login', accountInfo, headers).share();
    seq.subscribe((res: any) => {
      console.log(res);
      if (res.auth == 'yes') {
        console.log(res);
        this.storage.set('id', res.user_data[0].id );
        this.storage.set('correo', res.user_data[0].correo);
        this.storage.set('nombre', res.user_data[0].nombre);
        this.storage.set('usuario', res.user_data[0].usuario);
        this.storage.set('permiso', res.user_data[0].permiso);
        this.storage.set('puesto', res.user_data[0].puesto);
        this.storage.set('status', res.user_data[0].status);
        this.storage.set('loginSession', 'true');
        console.log("Datos cargados a local storage!");
      } else if (res.auth == 'no'){
        this.storage.set('loginSession', 'false');
      }
    }, err => {
      console.error('ERROR', err);
      this.storage.set('loginSession', 'false');
    });
    return seq;
  }

  /**
   * Logout session
   */
  logout() {
    this.storage.set('id', '');
    this.storage.set('correo', '');
    this.storage.set('nombre', '');
    this.storage.set('usuario', '');
    this.storage.set('permiso', '');
    this.storage.set('puesto', '');
    this.storage.set('status', '');
    this.storage.set('loginSession', 'false');
    console.log("Listo para salir!");
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(resp) {
    this._user = resp.user;
    console.log(this._user);
  }

}
