import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class RestProvider {

  datos : any;
  apiUrl : string = 'http://wms.dbpropan.com/api/admin'; //Production
  //apiUrl : string = 'http://rebelbot.mx/wms_ppo_ws/api/admin'; //Development Rebelbot
  //apiUrl : string = 'http://localhost/wms_ppo_ws/api/admin'; //Local


  constructor(public http: HttpClient) {
    console.log('Hello RestProvider Provider');
  }

  updateUsuario(user_id, permiso, usuario){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      id: usuario.id,
      usuario: usuario.usuario,
      password: usuario.password,
      nombre: usuario.nombre,
      departamento: usuario.departamento,
      sucursal: usuario.sucursal,
      correo: usuario.correo,
      puesto: usuario.puesto,
      status: usuario.status,
      permiso_user: usuario.permiso
    }
    console.log(datos);
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/edit_user",JSON.stringify(datos),options)
  }

  insertUsuario(user_id, permiso, usuario){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      usuario: usuario.usuario,
      password: usuario.password,
      nombre: usuario.nombre,
      departamento: usuario.departamento,
      correo: usuario.email,
      puesto: usuario.puesto,
      sucursal: usuario.sucursal,
      status: usuario.estado,
      permiso_user: usuario.permiso
    }

    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/add_user",JSON.stringify(datos),options)
  }

  getUsuario(user_id, permiso, usuario){
    let datos = { user_id:user_id,
                  permiso:permiso,
                  usuario_id: usuario}
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/get_user",JSON.stringify(datos),options)
  }

  delUsuario(user_id, permiso, usuario){
    let datos = { user_id:user_id,
                  permiso:permiso,
                  usuario: usuario}
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/delete_usuario",JSON.stringify(datos),options)
  }

  loadUsuarios(user_id, permiso){
    let datos = { user_id:user_id,permiso:permiso}
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/users",JSON.stringify(datos),options)
  }

  loadCode(user_id, permiso, code){
    let datos = { user_id:user_id, permiso:permiso, code:code.codigo}
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/exist_code",JSON.stringify(datos),options)
  }

  loadLote(user_id, permiso, code){
    let datos = { user_id:user_id, permiso:permiso, code:code.codigo, lote:code.lote}
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/exists_lote",JSON.stringify(datos),options)
  }


  loadAlmacenes(user_id, permiso){
    let datos = { user_id:user_id,permiso:permiso}
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/almacenes",JSON.stringify(datos),options)
  }

  existsAlmacen(user_id, permiso,almacen,ubicacion){
    return this.http.get(this.apiUrl+"/exists_ubicacion?user_id="+user_id+"&permiso="+permiso+"&almacen="+almacen+"&ubicacion="+ubicacion)
  }

  loadSucursales(user_id, permiso){
    let datos = { user_id:user_id,permiso:permiso}
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/sucursales",JSON.stringify(datos),options)
  }

  insertUbicacion(user_id, permiso, ubicacion, detail){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      almacen:ubicacion.almacen,
      ubicacion:ubicacion.ubicacion,
      pasillo:detail.pasillo,
      modulo:detail.modulo,
      nivel:detail.nivel,
      alto:detail.alto,
      ancho:detail.ancho,
      fondo:detail.fondo,
      zona:detail.zona,
      picking:detail.picking_val,
      montacargas:detail.montacargas_val,
      piso:detail.piso_val,
      seccion:detail.seccion
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/add_ubicacion",JSON.stringify(datos),options)
  }

  deleteUbicacion(user_id, permiso,almacen,ubicacion){
    let datos = { user_id:user_id,permiso:permiso,almacen:almacen,ubicacion:ubicacion }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/delete_ubicacion",JSON.stringify(datos),options)
  }

  getUbicacion(user_id, permiso,almacen,ubicacion){
    return this.http.get(this.apiUrl+"/get_ubicacion?user_id="+user_id+"&permiso="+permiso+"&almacen="+almacen+"&ubicacion="+ubicacion)
  }

  getUbicacionAlmacen(user_id, permiso,almacen){
    return this.http.get(this.apiUrl+"/get_ubicacionalmacen?user_id="+user_id+"&permiso="+permiso+"&almacen="+almacen)
  }

  updateUbicacion(user_id, permiso,ubicacion,detail){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      almacen:ubicacion.almacen,
      ubicacion:ubicacion.ubicacion,
      pasillo:detail.pasillo,
      modulo:detail.modulo,
      nivel:detail.nivel,
      alto:detail.alto,
      ancho:detail.ancho,
      fondo:detail.fondo,
      zona:detail.zona,
      picking:detail.picking_val,
      montacargas:detail.montacargas_val,
      piso:detail.piso_val,
      seccion:detail.seccion
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/update_ubicacion",JSON.stringify(datos),options)
  }

  loadSearchCode(user_id, permiso,text){
    return this.http.get(this.apiUrl+"/exists_code?text="+text+"&user_id="+user_id+"&permiso="+permiso)
  }

  loadSearchCodeInRelation(user_id, permiso,text){
    return this.http.get(this.apiUrl+"/exists_code_rel?text="+text+"&user_id="+user_id+"&permiso="+permiso)
  }

  loadSearchCodeInLote(user_id, permiso,text){
    return this.http.get(this.apiUrl+"/exists_code_lote?text="+text+"&user_id="+user_id+"&permiso="+permiso)
  }

  existsLote(user_id, permiso,codigo,lote){
    return this.http.get(this.apiUrl+"/exists_lote?user_id="+user_id+"&permiso="+permiso+"&codigo="+codigo+"&lote="+lote)
  }

  deleteLote(user_id, permiso,codigo,lote){
    let datos = { user_id:user_id,permiso:permiso,codigo:codigo,lote:lote }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/delete_lote",JSON.stringify(datos),options)
  }

  insertLote(user_id, permiso, lote, detail){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      codigo:lote.codigo,
      lote:lote.lote,
      alto:detail.alto,
      ancho:detail.ancho,
      largo:detail.largo,
      peso:detail.peso,
      ano:detail.ano,
      fecha:detail.fecha,
      master:detail.master,
      inner:detail.inner
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/add_lote",JSON.stringify(datos),options)
  }

  getLote(user_id, permiso,codigo,lote){
    return this.http.get(this.apiUrl+"/get_lote?user_id="+user_id+"&permiso="+permiso+"&codigo="+codigo+"&lote="+lote)
  }

  updateLote(user_id, permiso,lote,detail){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      codigo:lote.codigo,
      lote:lote.lote,
      alto:detail.alto,
      ancho:detail.ancho,
      largo:detail.largo,
      peso:detail.peso,
      ano:detail.ano,
      fecha:detail.fecha,
      master:detail.master,
      inner:detail.inner
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/update_lote",JSON.stringify(datos),options)
  }

  loadLotes(user_id, permiso,codigo){
    return this.http.get(this.apiUrl+"/code_lotes?user_id="+user_id+"&permiso="+permiso+"&codigo="+codigo)
  }

  existsRelacion(user_id, permiso,almacen,ubicacion,codigo,lote){
    return this.http.get(this.apiUrl+"/exists_relacion?user_id="+user_id+"&permiso="+permiso+"&almacen="+almacen
    +"&ubicacion="+ubicacion+"&codigo="+codigo+"&lote="+lote)
  }

  insertRelacion(user_id, permiso, relaciones){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      almacen:relaciones.almacen,
      ubicacion:relaciones.ubicacion,
      codigo:relaciones.codigo,
      lote:relaciones.lote
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/add_relacion",JSON.stringify(datos),options)
  }

  deleteRelacion(user_id, permiso,relaciones){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      almacen:relaciones.almacen,
      ubicacion:relaciones.ubicacion,
      codigo:relaciones.codigo,
      lote:relaciones.lote
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/delete_relacion",JSON.stringify(datos),options)
  }

  loadPaqueterias(user_id, permiso){
    let datos = { user_id:user_id,permiso:permiso}
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/paqueterias",JSON.stringify(datos),options)
  }

  insertPaqueteria(user_id, permiso, sucursal, paqueteria){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      sucursal:sucursal,
      paqueteria:paqueteria.paqueteria,
      direccion:paqueteria.direccion,
      contacto:paqueteria.contacto,
      telefono:paqueteria.telefono,
      celular:paqueteria.celular,
      correo: paqueteria.correo
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/add_paqueteria",JSON.stringify(datos),options)
  }

  deletePaqueteria(user_id, permiso,id){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      id:id
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/delete_paqueteria",JSON.stringify(datos),options)
  }

  updatePaqueteria(user_id, permiso, sucursal, paqueteria){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      id: paqueteria.id,
      sucursal:sucursal,
      paqueteria:paqueteria.paqueteria,
      direccion:paqueteria.direccion,
      contacto:paqueteria.contacto,
      telefono:paqueteria.telefono,
      celular:paqueteria.celular,
      correo: paqueteria.correo
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/update_paqueteria",JSON.stringify(datos),options)
  }

  uploadAlmacen(user_id,permiso,data){
    var options = {};
    let body = new FormData();
    body.append('csvFile', data);
    body.append('user_id', user_id);
    body.append('permiso', permiso);
    return this.http.post(this.apiUrl+"/upload_almacenes",body,options)
  }

  uploadCodigos(user_id,permiso,data){
    var options = {};
    let body = new FormData();
    body.append('csvFile', data);
    body.append('user_id', user_id);
    body.append('permiso', permiso);
    return this.http.post(this.apiUrl+"/upload_codigos",body,options)
  }

  searchUbicaciones(user_id, permiso, ubicacion){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      ubicacion:ubicacion
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/search_ubicacion",JSON.stringify(datos),options)
  }

  searchCodesRel(user_id, permiso, almacen, ubicacion){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      ubicacion:ubicacion,
      almacen:almacen
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/search_codesrel",JSON.stringify(datos),options)
  }

  updateRelacion(user_id, permiso, relacion){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      id: relacion.id,
      almacen:relacion.almacen,
      codigo:relacion.codigo,
      ubicacion:relacion.ubicacion,
      lote:relacion.lote
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/update_relacion",JSON.stringify(datos),options)
  }

  loadGrupos(user_id, permiso){
    let datos = {
      user_id:user_id,
      permiso:permiso
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/grupos",JSON.stringify(datos),options)
  }

  loadClasificaciones(user_id, permiso,grupo){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      grupo:grupo
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/clasificaciones",JSON.stringify(datos),options)
  }

  loadCodes(user_id, permiso,conteo){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      catin:conteo.catin,
      opcion1:conteo.opcion1,
      opcion2:conteo.opcion2,
      opcion3:conteo.opcion3,
      grupo:conteo.grupo,
      clasificacion:conteo.clasificacion
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/codes",JSON.stringify(datos),options)
  }

  loadUsersConteo(user_id, permiso,almacen){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      almacen:almacen
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/users_conteo",JSON.stringify(datos),options)
  }

  addConteo(user_id, permiso,usuarios,codigos,conteo){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      almacen:conteo.almacen,
      comentario:conteo.comentario,
      nombre:conteo.nombre,
      usuarios:usuarios,
      codigos:codigos
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/add_conteo",JSON.stringify(datos),options)
  }

  uploadConteo(user_id,permiso,data,identificador){
    var options = {};
    let body = new FormData();
    body.append('csvFile', data);
    body.append('user_id', user_id);
    body.append('permiso', permiso);
    body.append('identificador', identificador);
    return this.http.post(this.apiUrl+"/upload_conteo",body,options)
  }

  loadConteos(user_id, permiso){
    let datos = {
      user_id:user_id,
      permiso:permiso
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/load_conteos",JSON.stringify(datos),options)
  }

  loadConteosUsuario1(user_id,permiso){
    let datos = {
      user_id:user_id,
      permiso:permiso
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/loadConteoUsuario1",JSON.stringify(datos),options)
  }

  loadOrdersUsuario1(user_id,permiso){
    let datos = {
      user_id:user_id,
      permiso:permiso
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/loadOrderUsuario1",JSON.stringify(datos),options)
  }

  loadConteoDetalles(user_id,permiso,identificador,usuario){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      identificador:identificador,
      usuario:usuario
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/loadConteoDetail",JSON.stringify(datos),options)
  }

  loadOrderDetalles(user_id,permiso,identificador,usuario){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      identificador:identificador,
      usuario:usuario
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/loadOrderDetail",JSON.stringify(datos),options)
  }

  initConteo(user_id,permiso,detalle,usuario){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      detalle:detalle,
      usuario:usuario
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/init_conteo",JSON.stringify(datos),options)
  }

  updateConteo(user_id,permiso,detalle,conteo,usuario){
    let datos = {
      user_id:user_id,
      permiso:permiso,
      detalle:detalle,
      conteo:conteo,
      usuario:usuario
    }
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/update_conteo",JSON.stringify(datos),options)
  }

  getPorcentajeAvanceAdmin(user_id,permiso,identificador){
    let datos = {
      user_id: user_id,
      permiso: permiso,
      identificador: identificador
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/getPorcentajeAvanceAdmin",JSON.stringify(datos),options)
  }

  getPorcentajeAvance(user_id,permiso,identificador){
    let datos = {
      user_id: user_id,
      permiso: permiso,
      identificador: identificador
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/getPorcentajeAvance",JSON.stringify(datos),options)
  }

  getPorcentajeAvance2(user_id,permiso,identificador){
    let datos = {
      user_id: user_id,
      permiso: permiso,
      identificador: identificador
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/getPorcentajeAvance2",JSON.stringify(datos),options)
  }

  getPorcentajeAvance3(user_id,permiso,identificador){
    let datos = {
      user_id: user_id,
      permiso: permiso,
      identificador: identificador
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/getPorcentajeAvance3",JSON.stringify(datos),options)
  }

  getPorcentajeAvance4(user_id,permiso,identificador){
    let datos = {
      user_id: user_id,
      permiso: permiso,
      identificador: identificador
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/getPorcentajeAvance4",JSON.stringify(datos),options)
  }

  deleteConteo(user_id,permiso,identificador){
    let datos = {
      user_id: user_id,
      permiso: permiso,
      identificador: identificador
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/deleteConteo",JSON.stringify(datos),options)
  }

  deleteOrder(user_id,permiso,identificador){
    let datos = {
      user_id: user_id,
      permiso: permiso,
      identificador: identificador
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/deleteOrder",JSON.stringify(datos),options)
  }

  modifyConteo(user_id,permiso,conteo){
    let datos = {
      user_id: user_id,
      permiso: permiso,
      conteo: conteo
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/updateConteo",JSON.stringify(datos),options)
  }

  modifyOrder(user_id,permiso,order){
    let datos = {
      user_id: user_id,
      permiso: permiso,
      order: order
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/updateOrder",JSON.stringify(datos),options)
  }

  sendEmail(user_id,permiso,identificador,email){
    let datos = {
      user_id: user_id,
      permiso: permiso,
      identificador: identificador,
      email: email
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/sendEmail",JSON.stringify(datos),options)
  }

  loadOrders(user_id,permiso){
    let datos = {
      user_id: user_id,
      permiso: permiso
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/getOrders",JSON.stringify(datos),options)
  }

  loadOrderItem(user_id,permiso,identificador,codigo){
    let datos = {
      user_id: user_id,
      permiso: permiso,
      identificador: identificador,
      codigo: codigo
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/getOrderRow",JSON.stringify(datos),options)
  }

  loadCodigoLotes(user_id,permiso,codigo){
    let datos = {
      user_id: user_id,
      permiso: permiso,
      codigo: codigo
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/getLotes",JSON.stringify(datos),options)
  }

  loadLotesUbicacion(user_id,permiso,codigo,lote){
    let datos = {
      user_id: user_id,
      permiso: permiso,
      codigo: codigo,
      lote: lote
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/getUbicacionLote",JSON.stringify(datos),options)
  }

  sendRowData(user_id,permiso,order,row,eliminar,usuario){
    console.log("Order en rest: ");
    console.log(order);
    console.log("row en rest: ");
    console.log(row);
    let datos = {
      id: order[0].id,
      user_id: user_id,
      usuario: usuario,
      permiso: permiso,
      almacen: order[0].almacen,
      codigo: order[0].codigo,
      identificador: order[0].identificador,
      lote: row.lote,
      ubicacion: row.ubicacion,
      piezas: row.piezas,
      cajas: row.cajas,
      resto: row.resto,
      reempaque: row.reempaque,
      comentario: row.comentario,
      eliminar: eliminar
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/saveRowOrder",JSON.stringify(datos),options)
  }

  sendRow2Data(user_id,permiso,order,row,usuario){
    console.log("Order en rest: ");
    console.log(order);
    console.log("row en rest: ");
    console.log(row);
    let datos = {
      id: order[0].id,
      user_id: user_id,
      usuario: usuario,
      permiso: permiso,
      almacen: order[0].almacen,
      codigo: order[0].codigo,
      identificador: order[0].identificador,
      piezas: row.piezas,
      cajas: row.cajas,
      resto: row.resto,
      reempaque: row.reempaque,
      comentario: row.comentario
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/saveRow2Order",JSON.stringify(datos),options)
  }

  initOrderUser(user_id,permiso,order,usuario){
    let datos = {
      user_id: user_id,
      permiso: permiso,
      identificador: order.identificador,
      usuario: usuario
    }
    let options = {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded'
      }
    };
    return this.http.post(this.apiUrl+"/initOrderUser",JSON.stringify(datos),options)
  }

}
