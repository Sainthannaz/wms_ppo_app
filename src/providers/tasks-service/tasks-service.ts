import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { ToastController } from 'ionic-angular';
/*
  Generated class for the TasksServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TasksServiceProvider {

  db: SQLiteObject = null;

  constructor(public toastCtrl:ToastController) {}

  // public methods

  setDatabase(db: SQLiteObject){
    if(this.db === null){
      this.db = db;
    }
  }

  createTable(){
  	console.log('Tabla REG creada por primera vez');
  	let toast = this.toastCtrl.create({
	        message: 'Tabla REG creada exitosamente',
	        duration: 3000,
	        position: 'bottom'
	      });
	  toast.present();
    let sql = 'CREATE TABLE IF NOT EXISTS REG(id INTEGER PRIMARY KEY AUTOINCREMENT, ID_doc TEXT, documento TEXT, status TEXT, fecha TEXT, ds TEXT, nombre TEXT, paqueteria TEXT, almacen TEXT, codigo TEXT, codigo_padre TEXT, ubicacion TEXT, lote TEXT, cantidad INTEGER, importe TEXT, horaini_1 TEXT, horafin_1 TEXT, horaini_2 TEXT, horafin_2 TEXT, horaini_3 TEXT, horafin_3 TEXT, horaini_4 TEXT, horafin_4 TEXT, usuario1 TEXT, usuario2 TEXT, usuario3 TEXT, usuario4 TEXT, paso1 INTEGER, paso2 INTEGER, paso3 INTEGER, paso4 INTEGER, cajas INTEGER, master INTEGER, resto INTEGER, total1 INTEGER, total2 INTEGER, total3 INTEGER, total4 INTEGER, coincidencia TEXT, reempaque TEXT, peso TEXT, cbm TEXT,  comentario_in TEXT, comentario_out TEXT, comentario_linea TEXT, imagen TEXT, identificador TEXT, piezasxcaja TEXT, total_line TEXT)';
    return this.db.executeSql(sql, []);
  }

  delete(reg: any){
    let sql = 'DELETE FROM REG WHERE id=?';
    return this.db.executeSql(sql, [reg.id]);
  }

  getAll(){
    let sql = 'SELECT * FROM REG';
    return this.db.executeSql(sql, [])
    .then(response => {
      let reg = [];
      for (let index = 0; index < response.rows.length; index++) {
        reg.push( response.rows.item(index) );
      }
      return Promise.resolve( reg );
    })
    .catch(error => Promise.reject(error));
  }

  create(reg: any){
    let sql = 'INSERT INTO REG(title, completed) VALUES(?,?)';
    return this.db.executeSql(sql, [reg.title, reg.completed]);
  }

  /*
  update(task: any){
    let sql = 'UPDATE tasks SET title=?, completed=? WHERE id=?';
    return this.db.executeSql(sql, [task.title, task.completed, task.id]);
  }

  
  */

}
