import { Component, ViewChild } from '@angular/core';
import { App, Config, Nav, Platform, AlertController, LoadingController, NavController,
  ToastController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { HomePage} from '../pages/pages';
import { DashboardPage} from '../pages/dashboard/dashboard';
import { User } from '../providers/user/user';
import { Network } from '@ionic-native/network';
import { NetworkProvider } from '../providers/network/network';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { TasksServiceProvider } from '../providers/tasks-service/tasks-service';

@Component({
  template: `<ion-menu [content]="content">
    <ion-header no-border>
      <ion-toolbar>        
        <ion-title>WMS-PROCTR</ion-title>
      </ion-toolbar>
    </ion-header>
    <ion-content class="side-menu-background">      
      <div class="splash-logo" style="margin-top: 10px;"></div>
      <div class="spacer" style="height:30px;" id="menu-spacer1"></div>      
      <div class="spacer" style="height:30px;" id="menu-spacer1"></div>
      <ion-list class="list-style" >
        <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
          <ion-icon name="{{p.icon}}"></ion-icon> &nbsp; &nbsp; {{p.title | translate}}
        </button>
        <button ion-item (click)="closeSession()"><ion-icon name="exit"></ion-icon> &nbsp; &nbsp;
          {{'MENU_CLOSE' | translate}}
        </button>
      </ion-list>      
    </ion-content>

  </ion-menu>
  <ion-nav #content [root]="rootPage"></ion-nav>`
})
export class MyApp {
  rootPage:any = HomePage;
  loader: any;
 @ViewChild(Nav) nav: Nav;

 userData: { uid: string, email: string } = {
    uid: '',
    email: ''
  };
  pages: any[] = [
    { title: 'DASHBOARD', component: 'DashboardPage', icon:'logo-buffer' }
  ]
  alertText: any = {};
  user_id:any;
  admin_menu:boolean;
  user_menu:boolean;
  permiso:any;
  nombre: any;
  usuario: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public events: Events,
    public network: Network, public networkProvider: NetworkProvider, public toastCtrl:ToastController, public tasksService: TasksServiceProvider,
    private translate: TranslateService, public alertCtrl: AlertController, public app: App, private sqlite: SQLite,
    private storage: Storage, public loadingCtrl: LoadingController, private config: Config, private user: User) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.createDB();
      this.networkProvider.initializeNetworkEvents();
      console.log("Network Provider:");
             // Offline event
          this.events.subscribe('network:offline', () => {
              alert('No hay conexión a red! '+this.network.type);  
              let toast = this.toastCtrl.create({
                message: this.network.type,
                duration: 3000,
                position: 'bottom'
              });
              toast.present();  
              console.log(this.network.type);  
          });

          // Online event
          this.events.subscribe('network:online', () => {
              alert('Acceso a red! '+this.network.type);   
              let toast = this.toastCtrl.create({
                message: this.network.type,
                duration: 3000,
                position: 'bottom'
              });
              toast.present();     
              console.log(this.network.type);  
          });
    });

    this.initTranslate();
    Promise.all([this.storage.get("id"), this.storage.get("permiso"), this.storage.get("nombre"), this.storage.get("usuario")]).then(values => {
        this.user_id = values[0]; console.log(this.user_id);        
        this.permiso = values[1]; console.log(this.permiso);
        this.nombre = values[2]; console.log(this.permiso);
        this.usuario = values[3]; console.log(this.permiso);
        if (this.permiso == 'admin'){
          console.log('El usuario es admin');
          this.admin_menu = true;
          this.user_menu = false;
        } else if (this.permiso == 'user'){
          console.log('El usuario es user');
          this.admin_menu = false;
          this.user_menu = true;
        }
      });
  }

  createDB(){
    this.sqlite.create({
      name: 'data.db',
      location: 'default' // the location field is required
    })
    .then((db) => {
      this.tasksService.setDatabase(db);
      let toast = this.toastCtrl.create({
          message: 'Base de datos creada exitosamente',
          duration: 3000,
          position: 'bottom'
        });
      toast.present();
      return this.tasksService.createTable();
    })    
    .catch(error =>{
      let toast = this.toastCtrl.create({
          message: '¡Ocurrio un error al crear la base de datos!',
          duration: 3000,
          position: 'bottom'
        });
      toast.present();
      console.error(error);
    });
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      if (browserLang === 'zh') {
        const browserCultureLang = this.translate.getBrowserCultureLang();

        if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
          this.translate.use('zh-cmn-Hans');
        } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
          this.translate.use('zh-cmn-Hant');
        }
      } else {
        this.translate.use(this.translate.getBrowserLang());
      }
    } else {
      this.translate.use('es'); // Set your language here
    }

    this.translate.get('ALERT_TITLE_EXIT').subscribe(title => {
      this.alertText.title = title;
    });

    this.translate.get('ALERT_MESSAGE_EXIT').subscribe(message => {
      this.alertText.message = message;
    });

    this.translate.get('ALERT_OK').subscribe(ok => {
      this.alertText.ok = ok;
    });

    this.translate.get('ALERT_CANCEL').subscribe(cancel => {
      this.alertText.cancel = cancel;
    });
    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });

    
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  closeSession(){
    let alert = this.alertCtrl.create({
      title: this.alertText.title,
      message: this.alertText.message,
      buttons: [
        {
          text: this.alertText.cancel,
          role: 'cancel',
          handler: () => {
            console.log('Cancel');
          }
        },
        {
          text: this.alertText.ok,
          handler: () => {
           this.user.logout();
           this.nav.setRoot(HomePage);
           console.log('Ok');
          }
        }
      ]
    });
    alert.present();
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Authenticating..."
    });
    this.loader.present();
  }
}
