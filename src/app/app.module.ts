import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { MyApp } from './app.component';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Printer, PrintOptions } from '@ionic-native/printer';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { SQLite } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';
import { File } from '@ionic-native/file';
import { Keyboard } from '@ionic-native/keyboard';

// Providers
import { TasksServiceProvider } from '../providers/tasks-service/tasks-service';
import { ConnectionProvider } from '../providers/connection/connection';
import { NetworkProvider } from '../providers/network/network';
import { PrintProvider } from '../providers/print/print';
import { RestProvider } from '../providers/rest/rest';
import { Items } from '../providers/items/items';
import { User } from '../providers/user/user';
import { Api } from '../providers/api/api';

// Pages
import { HomePage } from '../pages/home/home';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { UbicacionPage } from '../pages/ubicacion/ubicacion';
import { UbicacionDetailPage } from '../pages/ubicacion-detail/ubicacion-detail';
import { UbicacionModifyPage } from '../pages/ubicacion-modify/ubicacion-modify';
import { LotePage } from '../pages/lote/lote';
import { LoteDetailPage } from '../pages/lote-detail/lote-detail';
import { LoteModifyPage } from '../pages/lote-modify/lote-modify';
import { RelacionesPage } from '../pages/relaciones/relaciones';
import { AlmacenesPage } from '../pages/almacenes/almacenes';
import { PaqueteriaPage } from '../pages/paqueteria/paqueteria';
import { PaqueteriaDetailPage } from '../pages/paqueteria-detail/paqueteria-detail';
import { PaqueteriaModifyPage } from '../pages/paqueteria-modify/paqueteria-modify';
import { SearchUbicacionPage } from '../pages/search-ubicacion/search-ubicacion';
import { SearchCodigosPage } from '../pages/search-codigos/search-codigos';
import { SearchLotePage } from '../pages/search-lote/search-lote';
import { SearchAlmacenPage } from '../pages/search-almacen/search-almacen';
import { RelacionesModifyPage } from '../pages/relaciones-modify/relaciones-modify';
import { ConteoPage } from '../pages/conteo/conteo';
import { UsersModalPage } from '../pages/users-modal/users-modal';
import { FileConteoPage } from '../pages/file-conteo/file-conteo';
import { ConteoDetailPage } from '../pages/conteo-detail/conteo-detail';
import { ConteoAddPage } from '../pages/conteo-add/conteo-add';
import { PrinterListModalPage } from '../pages/printer-list-modal/printer-list-modal';
import { ConteoUpdatePage } from '../pages/conteo-update/conteo-update';
import { OrderUpdatePage } from '../pages/order-update/order-update';
import { OrderDetailPage } from '../pages/order-detail/order-detail';
import { OrderListPage } from '../pages/order-list/order-list';
import { OrderRowPage } from '../pages/order-row/order-row';
import { OrderRow2Page } from '../pages/order-row2/order-row2';
import { RelacionModalPage } from '../pages/relacion-modal/relacion-modal';

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    UbicacionPage,
    UbicacionDetailPage,
    UbicacionModifyPage,
    LotePage,
    LoteDetailPage,
    LoteModifyPage,
    RelacionesPage,
    AlmacenesPage,
    PaqueteriaPage,
    PaqueteriaDetailPage,
    PaqueteriaModifyPage,
    SearchUbicacionPage,
    SearchCodigosPage,
    SearchLotePage,
    SearchAlmacenPage,
    RelacionesModifyPage,
    ConteoPage,
    UsersModalPage,
    FileConteoPage,
    ConteoDetailPage,
    ConteoAddPage,
    ConteoUpdatePage,
    OrderUpdatePage,
    OrderDetailPage,
    OrderListPage,
    OrderRowPage,
    RelacionModalPage,
    OrderRow2Page,
    PrinterListModalPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    UbicacionPage,
    UbicacionDetailPage,
    UbicacionModifyPage,
    LotePage,
    LoteDetailPage,
    LoteModifyPage,
    RelacionesPage,
    AlmacenesPage,
    PaqueteriaPage,
    PaqueteriaDetailPage,
    PaqueteriaModifyPage,
    SearchUbicacionPage,
    SearchCodigosPage,
    SearchLotePage,
    SearchAlmacenPage,
    RelacionesModifyPage,
    ConteoPage,
    UsersModalPage,
    FileConteoPage,
    ConteoDetailPage,
    ConteoAddPage,
    ConteoUpdatePage,
    OrderUpdatePage,
    OrderDetailPage,
    OrderListPage,
    OrderRowPage,
    RelacionModalPage,
    OrderRow2Page,
    PrinterListModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FileTransfer,
    Printer,
    SQLite,
    Network,
    BluetoothSerial,
    PrintProvider,
    File,
    { provide: ErrorHandler, useClass: IonicErrorHandler, deps: [Storage] },
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Api,
    ConnectionProvider,
    Items,
    User,
    RestProvider,
    NetworkProvider,
    TasksServiceProvider,
    Keyboard
  ]
})
export class AppModule {}
