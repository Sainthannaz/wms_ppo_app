export interface Print {
  label1?: string;
  text1?: string;
  label2?: string;
  text2?: string;
  date?: string;
  quantity?: number;
}
