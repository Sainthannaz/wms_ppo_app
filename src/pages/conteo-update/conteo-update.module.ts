import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConteoUpdatePage } from './conteo-update';

@NgModule({
  declarations: [
    ConteoUpdatePage,
  ],
  imports: [
    IonicPageModule.forChild(ConteoUpdatePage),
  ],
})
export class ConteoUpdatePageModule {}
