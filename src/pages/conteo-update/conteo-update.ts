import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-conteo-update',
  templateUrl: 'conteo-update.html',
})
export class ConteoUpdatePage {

  user_id:any;
  permiso:any;
  conteo = {
    almacen:'',
    comentario_in:'',
    usuario1:'',
    usuario2:'',
    usuario3:'',
    usuario4:'',
    identificador:'',
    documento:''
  }
  users:any[];
  success:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage:Storage, public restProvider:RestProvider, public toastCtrl:ToastController) {
      this.conteo = navParams.data.conteo;
      Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
        this.loadUsers();
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConteoUpdatePage');
  }

  loadUsers(){
    this.restProvider.loadUsersConteo(this.user_id,this.permiso,this.conteo.almacen).subscribe(
      (res) => {
        this.success = res['auth'];
        this.users = res['users'];
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  updateField(){
    this.conteo.documento = this.conteo.documento.toUpperCase();
  }

  updateField2(){
    this.conteo.comentario_in = this.conteo.comentario_in.toUpperCase();
  }

  updateConteo(){
    console.log("Actualizar datos del conteo: ");
    console.log(this.conteo);
    this.restProvider.modifyConteo(this.user_id,this.permiso,this.conteo).subscribe(
      (res) => {
        console.log("Result de modify conteo");
        console.log(res);
        this.success = res['auth'];
        if(this.success=="yes"){
          this.navCtrl.pop();
        }else{
          let toast = this.toastCtrl.create({
            message: "Ocurrio un error al actualizar el conteo, intentelo nuevamente.",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

}
