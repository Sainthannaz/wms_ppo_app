import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { FormControl, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-relaciones-modify',
  templateUrl: 'relaciones-modify.html',
})
export class RelacionesModifyPage {

  user_id:any;
  permiso:any;
  codigo:any[];
  relaciones = {
    almacen:'',
    ubicacion:'',
    codigo:'',
    lote:''
  }
  relacionesform: FormGroup;
  lotes: any[];
  error = {ubicacion:'',lotes:''}
  success:any;
  loader: any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider:RestProvider, private storage:Storage,
    public toastCtrl:ToastController, public loadingCtrl: LoadingController) {
      this.relaciones = navParams.data.codigo;
      console.log("Relaciones modificar");
      console.log(this.relaciones);
      Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
        this.loadLotes();
      });
  }

  ngOnInit() {
    this.relacionesform = new FormGroup({
      almacen: new FormControl('', [Validators.required]),
      ubicacion: new FormControl('', [Validators.required, Validators.pattern('[0-9]{2}[.]{1}[0-9]{2}[.]{1}[0-9]{2}([.]{1}[0-9]{2})?'), Validators.minLength(8), Validators.maxLength(11)]),
      codigo: new FormControl('', [Validators.required]),
      lote: new FormControl('',[Validators.required])
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RelacionesModifyPage');
  }

  loadLotes(){
    this.restProvider.loadLotes(this.user_id,this.permiso,this.relaciones.codigo).subscribe(
      (res) => {
        console.log("Result del provider de lotes codigos");
        console.log(res);
        this.success = res['auth'];
        this.lotes = res['lotes'];
        if(this.lotes.length==0){
          this.error.lotes = "No hay lotes asociados al codigo";
        }else{
          this.error.lotes = "";
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  updateRelacion(){
    this.showLoader('Actualizando Relación, por favor espere!');
    console.log("Actualizar relacion");
    console.log(this.relaciones);
    this.restProvider.updateRelacion(this.user_id,this.permiso,this.relaciones).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        let toast = this.toastCtrl.create({
          message: "Se ha actualizado la relacion correctamente.",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
        this.navCtrl.pop();
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  private showLoadingHandler(message) {
    if (this.loader == null) {
        this.loader = this.loadingCtrl.create({
            content: message
        });
        this.loader.present();
    } else {
        this.loader.data.content = message;
    }
  }

  private hideLoadingHandler() {
    if (this.loader != null) {
        this.loader.dismiss();
        this.loader = null;
    }
  }

  public showLoader(message) {
     this.showLoadingHandler(message);
  }

  public hideLoader() {
     this.hideLoadingHandler();
  }
}
