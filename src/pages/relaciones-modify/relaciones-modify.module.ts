import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RelacionesModifyPage } from './relaciones-modify';

@NgModule({
  declarations: [
    RelacionesModifyPage,
  ],
  imports: [
    IonicPageModule.forChild(RelacionesModifyPage),
  ],
})
export class RelacionesModifyPageModule {}
