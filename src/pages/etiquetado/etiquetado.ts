import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ModalController, MenuController, LoadingController, AlertController, ToastController, TextInput } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Printer, PrintOptions } from '@ionic-native/printer';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { PrintProvider} from '../../providers/print/print';
import { PrinterListModalPage} from '../printer-list-modal/printer-list-modal';
import { PopoverController } from 'ionic-angular';
import { DatePipe } from '@angular/common';
import { Keyboard } from '@ionic-native/keyboard';

//interfaces
import { Print } from "../../interfaces/print.interface";

//providers
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-etiquetado',
  templateUrl: 'etiquetado.html',
  providers: [DatePipe]
})
export class EtiquetadoPage {
  @ViewChild('inputCodigo') inputCodigo: TextInput ;

  codigo:string = '';
  lote:string = '';
  orden:string = '';
  cajas:string = '';
  general: boolean;
  etiquetaCodigos: boolean;
  etiquetaPedidos: boolean;
  user_id:any;
  permiso:any;
  loader: any = null;
  selectedPrinter:any=[];
  dataPrint: Print;
  date:string;
  quantity:number = 1;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public restProvider:RestProvider,
              private printProvider:PrintProvider,
              public translateService: TranslateService,
              private storage:Storage,
              public menuCtrl:MenuController,
              private bluetoothSerial: BluetoothSerial,
              private alertCtrl:AlertController,
              public toastCtrl:ToastController,
              public loadingCtrl: LoadingController,
              private printer: Printer,
              private modalCtrl:ModalController,
              private popoverCtrl:PopoverController,
              private datePipe: DatePipe,
              private keyboard: Keyboard ) {

      this.date = this.datePipe.transform(this.date, 'dd-MM-yyyy');

      console.log( this.date );

      Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
      });

    this.general = true;
    console.log(this.selectedPrinter);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EtiquetadoPage');
  }

  private showLoadingHandler(message) {
  	if (this.loader == null) {
	        this.loader = this.loadingCtrl.create({
	            content: message
	        });
	        this.loader.present();
	    } else {
	        this.loader.data.content = message;
	    }
	}

  private hideLoadingHandler() {
      if (this.loader != null) {
          this.loader.dismiss();
          this.loader = null;
      }
  }

  public showLoader(message) {
      this.showLoadingHandler(message);
  }

  public hideLoader() {
      this.hideLoadingHandler();
  }

  eCodigos(){
  	this.general = false;
  	this.etiquetaCodigos = true;
  	this.etiquetaPedidos = false;
    this.codigo = '';
    this.lote = '';
    this.date = '';
  }

  ePedidos(){
  	this.general = false;
  	this.etiquetaCodigos = false;
  	this.etiquetaPedidos = true;
    this.orden = '';
    this.cajas = '';
    this.date = '';
  }

  cancel(){
  	this.general = true;
  	this.etiquetaCodigos = false;
  	this.etiquetaPedidos = false;
  }

  listBTDevice(){

    let toast = this.toastCtrl.create({
        message: "Se intentará abrir modal!",
        duration: 3000,
        position: 'bottom'
    });
    toast.present();

    this.printProvider.searchBt().then( datalist => {

      let modal = this.modalCtrl.create( PrinterListModalPage ,{ data:datalist });

      modal.onDidDismiss( data => {
        this.selectedPrinter = data;

        let alert = this.alertCtrl.create({
          title: data.name+" selected",
          buttons:['Dismiss']
        });
        alert.present();

      });

      modal.present();

    }, err => {

      console.log("ERROR",err);
      let alertError = this.alertCtrl.create({
        title:"ERROR "+err,
        buttons:['Dismiss']
      });
      alertError.present();

    })

  }

  testConnectPrinter(){
    var id=this.selectedPrinter.id;
    if(id==null||id==""||id==undefined)
    {
      let alertError = this.alertCtrl.create({
        title:"¡Impresora Bluetooth no seleccionada!",
        buttons:['Dismiss']
      });
      alertError.present();

      setTimeout(()=>{
          this.listBTDevice();
     }, 1500);
    }
    else
    {
      let foo=this.printProvider.connectBT(id).subscribe(data=>{
        console.log("CONNECT SUCCESSFUL",data);

        let mno=this.alertCtrl.create({
          title:"Connect successful",
          buttons:['Dismiss']
        });
        mno.present();

      },err=>{
        console.log("Not able to connect",err);
        let mno=this.alertCtrl.create({
          title:"ERROR "+err,
          buttons:['Dismiss']
        });
        mno.present();
      });
    }
  }

  Print()
  {
    if(this.etiquetaCodigos){

      console.log('entró en etiquetaCodigos');
      this.dataPrint = {
        label1 : "CÓDIGO",
        text1 : this.codigo,
        label2 : "LOTE",
        text2: this.lote,
        date: this.date,
        quantity: this.quantity
      };
      console.log(this.dataPrint);

    } else if(this.etiquetaPedidos){

      console.log('entró en etiquetaPedidos');
      this.dataPrint = {
        label1 : "ORDEN",
        text1 : this.orden,
        label2 : "CAJAS",
        text2: this.cajas,
        date: this.date,
        quantity: this.quantity
      };
      console.log(this.dataPrint);
    }

    var id=this.selectedPrinter.id;
    if(id==null||id==""||id==undefined)
    {
      let alertError = this.alertCtrl.create({
        title:"¡Impresora Bluetooth no seleccionada!",
        buttons:['Dismiss']
      });
      alertError.present();

      setTimeout(()=>{
          this.listBTDevice();
     }, 1500);
    }
    else
    {
      let foo = this.printProvider.Print(id, this.dataPrint);
    }

  }

  setFilteredItems(code:string) {
    return code.toUpperCase();
  }

  maskedDate(oldDate:string) {

    if(oldDate.length == 2){
      oldDate = oldDate + "/"
    }

    if(oldDate.length === 5){
      oldDate = oldDate + "/"
    }
    /*
    var arrayDate =  oldDate.split("/");
    var dateDay =  Number(arrayDate[0]);
    var dateMonth =  Number(arrayDate[1]);
    var dateYear =  Number(arrayDate[2]);

    if(isNaN(dateDay)){
      console.log("is not a number" + dateDay)
      this.date = null;
    } else {
      console.log("is  a number" + dateDay)
      if(dateDay>31){
        this.date = "";
      }
    }*/

    return oldDate;
  }

  checkBlur(){
    console.log( "SALIÓ" );
  }

  checkFocus(){
    console.log( "ENTRÓ" );
  }

  loadCode( codigo:string ){
    this.restProvider.loadSearchCode(this.user_id,this.permiso, codigo ).subscribe(
      ( data ) => {
        var result = data['result'];
        if( result.length == 0 ){
          this.codigo = "";
          
          let alertError = this.alertCtrl.create({
            title:"¡El código ingresado no existe!",
            buttons:['Dismiss']
          });
          alertError.present();
          alertError.onDidDismiss(res => {
            setTimeout(() => {
              this.keyboard.show();
              this.inputCodigo.setFocus();
            },150);
          });
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  increase( num: number ){
    this.quantity = this.quantity + 1;
  }

  decrease( num: number){
    if( this.quantity != 1 ){
      this.quantity = this.quantity - 1;
    }
  }
}
