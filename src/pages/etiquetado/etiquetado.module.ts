import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EtiquetadoPage } from './etiquetado';

@NgModule({
  declarations: [
    EtiquetadoPage,
  ],
  imports: [
    IonicPageModule.forChild(EtiquetadoPage),
  ],
})
export class EtiquetadoPageModule {}
