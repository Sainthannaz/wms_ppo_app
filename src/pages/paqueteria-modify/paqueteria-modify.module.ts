import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaqueteriaModifyPage } from './paqueteria-modify';

@NgModule({
  declarations: [
    PaqueteriaModifyPage,
  ],
  imports: [
    IonicPageModule.forChild(PaqueteriaModifyPage),
  ],
})
export class PaqueteriaModifyPageModule {}
