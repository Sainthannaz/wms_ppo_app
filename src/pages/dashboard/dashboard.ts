import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Platform } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { OrdenesPage } from '../pages';
import { DefinicionesPage } from '../pages';
import { BuscarPage } from '../pages';
import { ReportesPage } from '../pages';
import { UsuariosPage } from '../pages';
import { EtiquetadoPage } from '../pages';
import { InventarioPage } from '../pages';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { User } from '../../providers/user/user';
/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})



export class DashboardPage {
alertText: any = {};
user_id:any;
permiso:any;
nombre:any;
usuario:any;
ordenes: boolean;
definiciones:boolean;
buscar:boolean;
reportes:boolean;
imprimir:boolean;
gestion:boolean;
inventario:boolean;

public unregisterBackButtonAction: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform,
		public translateService: TranslateService, public menu: MenuController,
    private alertCtrl: AlertController, private storage: Storage, private user: User) {
  	this.translateService.get('ALERT_TITLE_EXIT').subscribe(title => {
      this.alertText.title = title;
    });

    this.translateService.get('ALERT_MESSAGE_EXIT').subscribe(message => {
      this.alertText.message = message;
    });

    this.translateService.get('ALERT_OK').subscribe(ok => {
      this.alertText.ok = ok;
    });

    this.translateService.get('ALERT_CANCEL').subscribe(cancel => {
      this.alertText.cancel = cancel;
    });
   

  }

  ionViewDidLoad() {    
    console.log('ionViewDidLoad DashboardPage');
      Promise.all([this.storage.get("id"), this.storage.get("permiso"), this.storage.get("nombre"), this.storage.get("usuario")]).then(values => {
      this.user_id = values[0]; console.log(this.user_id);        
      this.permiso = values[1]; console.log(this.permiso);
      this.nombre = values[2]; console.log(this.permiso);
      this.usuario = values[3]; console.log(this.permiso);
        if (this.permiso == "admin" || this.permiso == "ADMIN"){
          this.ordenes=true;
          this.definiciones=true;
          this.buscar=true;
          this.reportes=true;
          this.imprimir=true;
          this.gestion=true;
          this.inventario=true;
        } else if (this.permiso == "user" || this.permiso == "USER"){
          this.ordenes=true;
          this.definiciones=true;
          this.buscar=true;
          this.reportes=true;
          this.imprimir=true;
          this.gestion=false;
          this.inventario=true;
        }
      });
    
    
  }

  openOrders(){
    this.navCtrl.push(OrdenesPage);
  }

  openDefinitions(){
    this.navCtrl.push(DefinicionesPage);
  }

  openSearch(){
    this.navCtrl.push(BuscarPage);
  }

  openReports(){
    this.navCtrl.push(ReportesPage);
  }

  openPrinter(){
    this.navCtrl.push(EtiquetadoPage);
  }

  openUsers(){
    this.navCtrl.push(UsuariosPage);
  }

  openManagment(){

  }

  openInventory(){
    this.navCtrl.push(InventarioPage);
  }


  closeSession(){
	  let alert = this.alertCtrl.create({
	    title: this.alertText.title,
	    message: this.alertText.message,
	    buttons: [
	      {
	        text: this.alertText.cancel,
	        role: 'cancel',
	        handler: () => {
	          console.log('Cancel');
	        }
	      },
	      {
	        text: this.alertText.ok,
	        handler: () => {
           this.user.logout();
           this.navCtrl.setRoot(HomePage);
           //this.navCtrl.push(HomePage);
	         console.log('Ok');
	        }
	      }
	    ]
	  });
	  alert.present();
	} 

  ionViewDidEnter(){
     this.initializeBackButtonCustomHandler();
  }

  ionViewWillLeave() {
    // Unregister the custom back button action for this page
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  initializeBackButtonCustomHandler(): void {
      this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
        console.log('Prevent Back Button Page Change');
      }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }


}
