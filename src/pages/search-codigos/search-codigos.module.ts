import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchCodigosPage } from './search-codigos';

@NgModule({
  declarations: [
    SearchCodigosPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchCodigosPage),
  ],
})
export class SearchCodigosPageModule {}
