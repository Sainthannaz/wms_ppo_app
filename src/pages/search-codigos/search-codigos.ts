import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';
import { RelacionesModifyPage } from '../relaciones-modify/relaciones-modify';

/**
 * Generated class for the SearchCodigosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-codigos',
  templateUrl: 'search-codigos.html',
})
export class SearchCodigosPage {
  searchCode: string = '';
  searchControl: FormControl;
  searching: any = false;
  codes: any[];
  success:any;
  exists:any;
  user_id:any;
  permiso:any;
  showSearchCode:any = true;
  loader: any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public restProvider:RestProvider, private storage:Storage, public alertCtrl:AlertController,
    public toastCtrl:ToastController, public loadingCtrl: LoadingController) {
  	this.searchControl = new FormControl();
    this.showSearchCode = true;
    Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchCodigosPage');
    this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
      this.searching = false;
      this.setFilteredItems();
    });

    this.searchCode = '';
  }

  setFilteredItems() {
  	console.log('Vamos a buscar...');
  	console.log(this.searchCode);
    this.restProvider.loadSearchCodeInRelation(this.user_id,this.permiso,this.searchCode).subscribe(
      (res) => {
        console.log("Result del provider");
        console.log(res);
        this.success = res['auth'];
        this.codes = res['result'];
      },
      (error) =>{
        console.error(error);
      }
    )
    //this.items = this.dataService.filterItems(this.searchTerm);
  }

  onSearchInput(){
    this.searching = true;
  }

  codeSelected(code){
    console.log(code);
    //this.showSearchCode = false;
    console.log(code.codigo);
  }

  eliminar(codigo){
    this.restProvider.deleteRelacion(this.user_id,this.permiso,codigo).subscribe(
      (res) => {
        console.log("Result del provider");
        console.log(res);
        this.success = res['auth'];
        let toast = this.toastCtrl.create({
          message: "Se ha eliminado la relacion correctamente.",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();

        let index: number = this.codes.indexOf(codigo); // <-- todo?
        if(index > -1){
            this.codes.splice(index, 1);
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  modificar(codes){
  	console.log(codes.codigo);
    this.navCtrl.push(RelacionesModifyPage,{codigo:codes});
  }

  private showLoadingHandler(message) {
    if (this.loader == null) {
        this.loader = this.loadingCtrl.create({
            content: message
        });
        this.loader.present();
    } else {
        this.loader.data.content = message;
    }
  }

  private hideLoadingHandler() {
    if (this.loader != null) {
        this.loader.dismiss();
        this.loader = null;
    }
  }

  public showLoader(message) {
     this.showLoadingHandler(message);
  }

  public hideLoader() {
     this.hideLoadingHandler();
  }
}
