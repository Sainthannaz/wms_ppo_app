import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { FormControl, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-relacion-modal',
  templateUrl: 'relacion-modal.html',
})
export class RelacionModalPage {

  user_id:any;
  permiso:any;
  relacionesform: FormGroup;
  success:string;
  relacion = {
    lote: '',
    ubicacion: ''
  }
  order:any[];
  relaciones = {
    'almacen': '',
    'codigo': '',
    'lote': '',
    'ubicacion': ''
  }
  lotes:any[];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl:ViewController,public toastCtrl:ToastController,
    public restProvider:RestProvider, private storage:Storage,) {
      this.order = this.navParams.get('order');
      Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
      });
  }

  ngOnInit() {
    this.relacionesform = new FormGroup({
      ubicacion: new FormControl('', [Validators.required, Validators.pattern('[0-9]{2}[.]{1}[0-9]{2}[.]{1}[0-9]{2}([.]{1}[0-9]{2})?'), Validators.minLength(8), Validators.maxLength(11)]),
      lote: new FormControl('',[Validators.required])
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RelacionModalPage');
  }

  closeModal(){
    if(this.relacion.lote!="" && this.relacion.ubicacion!=""){
      if(this.relacionesform.valid){
        this.relaciones.almacen = this.order[0].almacen;
        this.relaciones.codigo = this.order[0].codigo;
        this.relaciones.lote = this.relacion.lote;
        this.relaciones.ubicacion = this.relacion.ubicacion;
        this.restProvider.insertRelacion(this.user_id,this.permiso,this.relaciones).subscribe(
          (res) => {
            this.success = res['auth'];
            if(this.success=="yes"){
              let toast = this.toastCtrl.create({
                message: "Relación agregada correctamente",
                duration: 5000,
                position: 'bottom'
              });
              toast.present();
              this.loadCodigoLotes();

            }
          },
          (error) =>{
            console.error(error);
          }
        )
      }else{
        let toast = this.toastCtrl.create({
          message: "La ubicación no tiene el formato correcto ##.##.##.",
          duration: 5000,
          position: 'bottom'
        });
        toast.present();
      }
        //this.viewCtrl.dismiss(this.usuarios);
    }else{
      if(!this.relacionesform.valid){
        let toast = this.toastCtrl.create({
          message: "Debe ingresar un lote y su ubicacion.",
          duration: 5000,
          position: 'bottom'
        });
        toast.present();
      }else{
        alert("No hay error");
      }
    }
  }

  loadCodigoLotes(){
    this.restProvider.loadCodigoLotes(this.user_id,this.permiso,this.order[0].codigo).subscribe(
      (res) => {
        this.success = res['auth'];
        if(this.success=="yes"){
          this.lotes = res['lotes'];
          this.viewCtrl.dismiss(this.lotes);
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

}
