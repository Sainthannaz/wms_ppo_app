import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RelacionModalPage } from './relacion-modal';

@NgModule({
  declarations: [
    RelacionModalPage,
  ],
  imports: [
    IonicPageModule.forChild(RelacionModalPage),
  ],
})
export class RelacionModalPageModule {}
