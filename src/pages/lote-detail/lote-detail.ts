import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-lote-detail',
  templateUrl: 'lote-detail.html',
})
export class LoteDetailPage {
  alto: number;
  ancho:number;
  largo: number;
  cbm: number;
  lote = {codigo:'',lote:''}
  user_id:any;
  permiso:any;
  success:any;
  detail = {
    alto:'',
    ancho:'',
    largo:'',
    peso:'',
    master:'',
    inner:'',   
    ano:'',
    fecha:''
  }
  loader: any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider:RestProvider, private storage:Storage, public toastCtrl:ToastController,
    public loadingCtrl: LoadingController) {
    this.lote = navParams.data.lote;
    Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
      this.user_id = values[0];
      this.permiso = values[1];
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoteDetailPage');
  }

  insertLote(){
    this.showLoader('Insertando Lote, por favor espere!');
    this.restProvider.insertLote(this.user_id,this.permiso,this.lote,this.detail).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          if(res['result']!=0){
            let toast = this.toastCtrl.create({
    	        message: "Se ha registrado el lote correctamente. Registro: "+res['result'],
    	        duration: 3000,
    	        position: 'bottom'
    	      });
    	      toast.present();
            this.navCtrl.pop();
          }else{
            let toast = this.toastCtrl.create({
    	        message: "Ocurrio un error al guardar la información, intentelo nuevamente.",
    	        duration: 3000,
    	        position: 'bottom'
    	      });
    	      toast.present();
          }
        }
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  private showLoadingHandler(message) {
    if (this.loader == null) {
        this.loader = this.loadingCtrl.create({
            content: message
        });
        this.loader.present();
    } else {
        this.loader.data.content = message;
    }
  }

  private hideLoadingHandler() {
    if (this.loader != null) {
        this.loader.dismiss();
        this.loader = null;
    }
  }

  public showLoader(message) {
     this.showLoadingHandler(message);
  }

  public hideLoader() {
     this.hideLoadingHandler();
  }
}
