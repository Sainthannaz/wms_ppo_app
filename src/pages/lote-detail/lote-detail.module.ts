import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoteDetailPage } from './lote-detail';

@NgModule({
  declarations: [
    LoteDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(LoteDetailPage),
  ],
})
export class LoteDetailPageModule {}
