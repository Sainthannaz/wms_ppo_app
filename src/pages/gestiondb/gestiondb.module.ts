import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GestiondbPage } from './gestiondb';

@NgModule({
  declarations: [
    GestiondbPage,
  ],
  imports: [
    IonicPageModule.forChild(GestiondbPage),
  ],
})
export class GestiondbPageModule {}
