import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { UsuariosPage } from './usuarios';

@NgModule({
  declarations: [
    UsuariosPage,
  ],
  imports: [
    IonicPageModule.forChild(UsuariosPage),
    TranslateModule.forChild()
  ],
})
export class UsuariosPageModule {}
