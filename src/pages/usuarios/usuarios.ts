import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, MenuController, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';
import { FormControl, FormGroup, Validators } from '@angular/forms';
/**
 * Generated class for the UsuariosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-usuarios',
  templateUrl: 'usuarios.html',
})
export class UsuariosPage {
  user_id:any;
  permiso:any;
  success:any;
  exists:any;
  sucursales:any[];
  usuario:any[];
  usuarios:any[];
  general: boolean;
  alta: boolean;
  modificacion: boolean;
  alertText: any = {};
  saveUserForm: FormGroup;
  editUserForm: FormGroup;
  account: { estado: 0, nombre: string, sucursal:string, usuario: string, password: string, departamento: string, email: string, puesto: string, permiso: string} = {
    	sucursal: '',
      nombre: '',
    	usuario: '',
    	password: '',
    	departamento: '',
    	email: '',
    	puesto: '',
    	permiso: '',
      estado: 0
  };

  permisos: any[] = [{  
    nombre: 'ADMIN'
  }, {   
    nombre: 'USER'
  }];

  status: any[] = [{
    status: 1,
    nombre: 'Activo'
  }, {
    status:2,
    nombre: 'Bloqueado'
  }];
  data:any[];
  loader: any = null;
  constructor(public navCtrl: NavController, public navParams: NavParams, public restProvider:RestProvider, public platform: Platform,
    public translateService: TranslateService, private storage:Storage, public menuCtrl:MenuController,
    private alertCtrl:AlertController, public toastCtrl:ToastController, public loadingCtrl: LoadingController) {
  	this.general = true;
    this.translateService.get('ALERT_TITLE_DELETE_USER').subscribe(title => {
      this.alertText.title = title;
    });
    this.translateService.get('ALERT_MESSAGE_DELETE_USER').subscribe(message => {
      this.alertText.message = message;
    });
    this.translateService.get('ALERT_OK').subscribe(ok => {
      this.alertText.ok = ok;
    });
    this.translateService.get('ALERT_CANCEL').subscribe(cancel => {
      this.alertText.cancel = cancel;
    });
  	Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
        this.loadSucursales();
        this.loadUsuarios();
      });
     platform.registerBackButtonAction(() => {
      console.log("backPressed 1");
    },1);
  }

   ngOnInit() {
    this.saveUserForm = new FormGroup({
      usuarioValidator: new FormControl('', [Validators.required]),
      sucursalValidator: new FormControl('', [Validators.required]),
      passwordValidator: new FormControl('', [Validators.required, Validators.minLength(5)]),
      nombreValidator: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      departamentoValidator: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      emailValidator: new FormControl(''),
      puestoValidator: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      permisoValidator: new FormControl('', [Validators.required]),
      statusValidator: new FormControl('', [Validators.required])
    });

    this.editUserForm = new FormGroup({
      eusuarioValidator: new FormControl('', [Validators.required]),
      esucursalValidator: new FormControl('', [Validators.required]),
      epasswordValidator: new FormControl('', [Validators.required, Validators.minLength(5)]),
      enombreValidator: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      edepartamentoValidator: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      eemailValidator: new FormControl(''),
      epuestoValidator: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      epermisoValidator: new FormControl('', [Validators.required]),
      estatusValidator: new FormControl('', [Validators.required])
    });

  }

  addUser(){
  	this.general = false;
  	this.alta = true;
  }

  cancelUser(){
  	this.alta = false;
  	this.general = true;
    this.modificacion = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UsuariosPage');
  }

  saveUser(){
    if(this.saveUserForm.valid){
      this.showLoader('¡Guardando, espere un momento!');
    	console.log(this.account);
      this.restProvider.insertUsuario(this.user_id,this.permiso, this.account).subscribe(
        (res) => {
          console.log(res);
          this.loadUsuarios();
          this.alta = false;
          this.modificacion = false;
          this.general = true;
          let toast = this.toastCtrl.create({
            message: "El usuario se guardo de forma exitosa!",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
          this.account.departamento = "";
          this.account.email = "";
          this.account.nombre = "";
          this.account.password = "";
          this.account.puesto = "";
          this.account.usuario = "";
          this.hideLoader();
        } ,
        (error) =>{
          console.error(error);
          this.hideLoader();
          let toast = this.toastCtrl.create({
            message: "Ocurrio un error al solicitar los datos!",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        }
      )
    }
  }

  updateUser(){
    this.showLoader('Actualizando, espere un momento!');
    console.log(this.usuario);
    this.restProvider.updateUsuario(this.user_id,this.permiso, this.usuario).subscribe(
      (res) => {
        console.log(res);
        this.loadUsuarios();
        this.alta = false;
        this.modificacion = false;
        this.general = true;
        this.hideLoader();
        let toast = this.toastCtrl.create({
          message: "Actualización exitosa!",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      } ,
      (error) =>{
        console.error(error);

        this.hideLoader();
      }
    )
  }

  loadSucursales(){
    this.restProvider.loadSucursales(this.user_id,this.permiso).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        if(this.success=="yes"){
          this.sucursales = res['sucursales'];
          console.log(this.sucursales);          
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

   loadUsuarios(){
    this.showLoader('Cargando...');
    this.restProvider.loadUsuarios(this.user_id,this.permiso).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        if(this.success=="yes"){
        	this.usuarios = res['users'];
          this.data = res['users'];
          this.hideLoader();
        }
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  loadUsuario(user){    
    this.showLoader('Cargando...');
     this.restProvider.getUsuario(this.user_id,this.permiso, user).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        if(this.success=="yes"){
          this.general = false;
          this.alta = false;
          this.modificacion = true;
          this.usuario = res['user'][0];
          console.log(this.usuario);
          this.hideLoader();
          let toast = this.toastCtrl.create({
            message: "Carga exitosa de los datos de usuario!",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        }
      },
      (error) =>{
        console.error(error);
        let toast = this.toastCtrl.create({
          message: "Ocurrio un error al solicitar los datos!",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
        this.hideLoader();
      }
    )
  }
 
  deleteUser(user){
    console.log(user);
    let alert = this.alertCtrl.create({
      title: this.alertText.title,
      message: this.alertText.message  + ': ' + user,
      buttons: [
        {
          text: this.alertText.cancel,
          role: 'cancel',
          handler: () => {
            console.log('Cancel');
          }
        },
        {
          text: this.alertText.ok,
          handler: () => { 
            this.showLoader('Borrando...');
             this.restProvider.delUsuario(this.user_id,this.permiso, user).subscribe(
              (res) => {
                console.log(res);
                this.success = res['auth'];
                if(this.success=="yes"){
                  this.general = true;
                  this.alta = false;
                  this.modificacion = false; 
                  this.hideLoader();
                  let toast = this.toastCtrl.create({
                    message: "Se elimino exitosamene el usuario!",
                    duration: 3000,
                    position: 'bottom'
                  });
                  toast.present();
                  this.loadUsuarios();
                }
              },
              (error) =>{
                console.error(error);
                let toast = this.toastCtrl.create({
                  message: "Ocurrio un error al solicitar el borrado!",
                  duration: 3000,
                  position: 'bottom'
                });
                toast.present();
                this.hideLoader();
              }
            )
          }
        }
      ]
    });
    alert.present();
  }


  private showLoadingHandler(message) {
        if (this.loader == null) {
            this.loader = this.loadingCtrl.create({
                content: message
            });
            this.loader.present();
        } else {
            this.loader.data.content = message;
        }
    }

    private hideLoadingHandler() {
        if (this.loader != null) {
            this.loader.dismiss();
            this.loader = null;
        }
    }

    public showLoader(message) {
        this.showLoadingHandler(message);
    }

    public hideLoader() {
        this.hideLoadingHandler();
    }

    doRefresh(refresher) {
      console.log('Begin async operation', refresher);
      setTimeout(() => {
        console.log('Async operation has ended');
        this.loadUsuarios();
        this.loadSucursales();
        refresher.complete();
      }, 2000);
    }

  filterItems(value){
    this.usuarios = this.data;
    this.usuarios = this.usuarios.filter(item => item.sucursal === value);
  }

  toUpperCaseUsuario(){
    console.log("Entro a upperCase Usuario");
    this.account.usuario = this.account.usuario.toUpperCase();
  }

  toUpperCaseNombre(){
    console.log("Entro a upperCase Nombre");
    this.account.nombre = this.account.nombre.toUpperCase();
  }

  toUpperCaseDepartamento(){
    console.log("Entro a upperCase Departamento");
    this.account.departamento = this.account.departamento.toUpperCase();
  }

  toUpperCaseEmail(){
    console.log("Entro a upperCase Email");
    this.account.email = this.account.email.toUpperCase();
  }

  toUpperCasePuesto(){
    console.log("Entro a upperCase Puesto");
    this.account.puesto = this.account.puesto.toUpperCase();
  }

}
