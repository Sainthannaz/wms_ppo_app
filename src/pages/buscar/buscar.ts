import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { SearchUbicacionPage } from '../search-ubicacion/search-ubicacion';
import { SearchCodigosPage } from '../search-codigos/search-codigos';
import { SearchLotePage } from '../search-lote/search-lote';
import { SearchAlmacenPage } from '../search-almacen/search-almacen';

@IonicPage()
@Component({
  selector: 'page-buscar',
  templateUrl: 'buscar.html',
})
export class BuscarPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public translateService: TranslateService) {
  }

  onCancelSearch(){

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuscarPage');
  }

  openSerchUbicacion(){
    this.navCtrl.push(SearchUbicacionPage);
  }

  openUbicacionesCodigo(){
    this.navCtrl.push(SearchCodigosPage);
  }

  openLotesCodigo(){
    this.navCtrl.push(SearchLotePage);

  }

  openUbicacionAlmacen(){
    this.navCtrl.push(SearchAlmacenPage);
  }

}
