import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuscarPage } from './buscar';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    BuscarPage,
  ],
  imports: [
    IonicPageModule.forChild(BuscarPage),
    TranslateModule.forChild()
  ],
})
export class BuscarPageModule {}
