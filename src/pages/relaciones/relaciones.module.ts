import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RelacionesPage } from './relaciones';

@NgModule({
  declarations: [
    RelacionesPage,
  ],
  imports: [
    IonicPageModule.forChild(RelacionesPage),
  ],
})
export class RelacionesPageModule {}
