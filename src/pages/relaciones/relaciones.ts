import { Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController, ViewController, TextInput  } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { FormControl, FormGroup, Validators } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-relaciones',
  templateUrl: 'relaciones.html',
})

export class RelacionesPage {
  @ViewChild('input') myInput: TextInput;
  user_id:any;
  permiso:any;
  relacionesform: FormGroup;
  success:string;
  exists:any;
  relaciones = {
    almacen:'',
    ubicacion:'',
    codigo:'',
    lote:''
  }
  
  almacenantiguo:string;
  almacenes:any[];
  error = {ubicacion:'',lotes:''}
  validUbicacion: any = false;
  searchCode: string = '';
  searchControl: FormControl;
  searching: any = false;
  showSearchCode:any = true;
  codes: any[];
  lotes: any[];
  loader: any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider:RestProvider, private storage:Storage, public toastCtrl:ToastController,
    public alertCtrl:AlertController, public loadingCtrl: LoadingController, private viewCtrl: ViewController) {
      this.searchControl = new FormControl();
      this.showSearchCode = true;
      Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
        this.loadAlmacenes();
      });
      
  }

  ngOnInit() {
    this.relacionesform = new FormGroup({
      almacen: new FormControl('', [Validators.required]),
      ubicacion: new FormControl('', [Validators.required, Validators.pattern('[0-9]{2}[.]{1}[0-9]{2}[.]{1}[0-9]{2}([.]{1}[0-9]{2})?'), Validators.minLength(8), Validators.maxLength(11)]),
      codigo: new FormControl('', [Validators.required]),
      lote: new FormControl('',[Validators.required])
    });

  }

  ionViewDidLoad() {
    
 }

  loadAlmacenes(){
    this.showLoader('¡Cargando Almacenes, por favor espere!');
    this.restProvider.loadAlmacenes(this.user_id,this.permiso).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          this.almacenes = res['almacenes'];
        }
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  validUbicacionAlmacen(){
    if(this.relaciones.almacen!="" && this.relaciones.ubicacion!=""){
      this.restProvider.existsAlmacen(this.user_id,this.permiso,this.relaciones.almacen,this.relaciones.ubicacion).subscribe(
        (res) => {
          console.log(res);
          this.success = res['auth'];
          if(this.success=="yes"){
              this.exists = res['exists'];
              if(this.exists=="no"){
                this.error.ubicacion = "Ubicación no relacionada";
              }else{
                this.error.ubicacion = "";
                this.validUbicacion = true;
              }
          }
          //this.total = res['total'];
        },
        (error) =>{
          console.error(error);
        }
      )
    }
  }

  onSearchInput(){
      this.searching = true;
  }

  setFilteredItems() {
    if(this.searchCode!=""){
      this.restProvider.loadSearchCode(this.user_id,this.permiso,this.searchCode).subscribe(
        (res) => {
          console.log("Result del provider");
          console.log(res);
          this.success = res['auth'];
          this.codes = res['result'];
        },
        (error) =>{
          console.error(error);
        }
      )
      //this.items = this.dataService.filterItems(this.searchTerm);
    }

  }

  codeSelected(code){
    console.log(code);
    this.showSearchCode = false;
    this.relaciones.codigo = code.codigo;
    this.codes = [];
    this.restProvider.loadLotes(this.user_id,this.permiso,this.relaciones.codigo).subscribe(
      (res) => {
        console.log("Result del provider de lotes codigos");
        console.log(res);
        this.success = res['auth'];
        this.lotes = res['lotes'];
        if(this.lotes.length==0){
          this.error.lotes = "No hay lotes asociados al codigo";
          this.showSearchCode = true;
        }else{
          this.error.lotes = "";
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  existsRelacion(){
    if(this.relaciones.codigo.length <= 0){
      console.log("Esta vacio!");
    } else {
      this.showLoader('Validando Relación, por favor espere!');
      console.log("Entro a validar si existe la relacion en la base");
      this.restProvider.existsRelacion(this.user_id,this.permiso,this.relaciones.almacen,this.relaciones.ubicacion,this.relaciones.codigo,this.relaciones.lote).subscribe(
        (res) => {
          console.log("Result del provider de exists relacion");
          console.log(res);
          this.success = res['auth'];
          this.hideLoader();
          this.exists = res['exists'];
          if(this.exists=="yes"){
            console.log("Ya existe la relacion");
            this.presentConfirm();
          }else{
            console.log("Entro a guardar la relación");
            this.insertRelacion();
          }
        },
        (error) =>{
          console.error(error);
          this.hideLoader();
        }
      )
    }
  }

  insertRelacion(){
    this.showLoader('Creando Relación, por favor espere!');
    this.restProvider.insertRelacion(this.user_id,this.permiso,this.relaciones).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          if(res['result']!=0){
            let toast = this.toastCtrl.create({
              message: "Se ha registrado la relación correctamente. Registro: "+res['result'],
              duration: 3000,
              position: 'bottom'
            });
            /*setTimeout(() => {
              this.relacionesform.reset();
            },500);*/
            this.almacenantiguo = this.relaciones.almacen;
            this.relaciones = {
              almacen: this.almacenantiguo,
              ubicacion:'',
              codigo:'',
              lote:''
            }
            toast.present();          
            //this.navCtrl.pop();
            this.searchControl = new FormControl();
            this.showSearchCode = true;
            this.relaciones.codigo = "";
            this.searchCode = "";
            this.viewCtrl._willEnter(); //<---- this is what dis the trick
            setTimeout(() => {
              this.myInput.setFocus();
            },150);

          }else{
            let toast = this.toastCtrl.create({
              message: "Ocurrio un error al guardar la información, intentelo nuevamente.",
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
          }
        }
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Relación existente',
      message: '¿Qué desea hacer?',
      buttons: [
        {
          text: 'Eliminar',
          handler: () => {
            this.deleteRelacion();
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
            //this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

  deleteRelacion(){
    this.showLoader('Eliminando Relación, por favor espere!');
    this.restProvider.deleteRelacion(this.user_id,this.permiso,this.relaciones).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          let toast = this.toastCtrl.create({
            message: "Se ha eliminado la relación correctamente.",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
          //this.navCtrl.pop();
          this.searchControl = new FormControl();
          this.showSearchCode = true;
          this.relaciones.codigo = "";
          //this.relaciones.almacen = "";
          this.relaciones.ubicacion = "";
          this.relaciones.lote = "";
          this.searchCode = "";
          this.viewCtrl._willEnter(); //<---- this is what dis the trick
        }else{
          let toast = this.toastCtrl.create({
            message: "Ha ocurrido un error al eliminar, intentelo nuevamente.",
            duration: 5000,
            position: 'bottom'
          });
          toast.present();
        }
        //this.total = res['total'];
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  ionViewWillEnter(){
    console.log('ionViewDidLoad RelacionesPage');
    this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
      this.searching = false;
      this.setFilteredItems();
    });
  }

  /*
  ionViewDidLoad() {
    console.log('ionViewDidLoad RelacionesPage');
    this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
      this.searching = false;
      this.setFilteredItems();
    });
  }
  */

  private showLoadingHandler(message) {
    if (this.loader == null) {
        this.loader = this.loadingCtrl.create({
            content: message
        });
        this.loader.present();
    } else {
        this.loader.data.content = message;
    }
  }

  private hideLoadingHandler() {
    if (this.loader != null) {
        this.loader.dismiss();
        this.loader = null;
    }
  }

  public showLoader(message) {
     this.showLoadingHandler(message);
  }

  public hideLoader() {
     this.hideLoadingHandler();
  }
}
