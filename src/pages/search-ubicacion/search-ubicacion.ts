import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { RelacionesModifyPage } from '../relaciones-modify/relaciones-modify';

@IonicPage()
@Component({
  selector: 'page-search-ubicacion',
  templateUrl: 'search-ubicacion.html',
})
export class SearchUbicacionPage {

  user_id:any;
  permiso:any;
  almacen: any;
  success:any;
  exists:any;
  almacenes:any[];
  ubicacion = {almacen:'',ubicacion:''}
  ubicacionform: FormGroup;
  pred_almacen: any;
  codigos:any[];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider:RestProvider, private storage:Storage, private toastCtrl:ToastController) {
      Promise.all([this.storage.get("id"), this.storage.get("permiso"), this.storage.get("almacen")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
        this.pred_almacen = values[2];
        this.loadAlmacenes();
      });
  }

  ngOnInit() {
    this.ubicacionform = new FormGroup({
      almacen: new FormControl('', [Validators.required]),
      ubicacion: new FormControl('', [Validators.required, Validators.pattern('[0-9]{2}[.]{1}[0-9]{2}[.]{1}[0-9]{2}([.]{1}[0-9]{2})?'), Validators.minLength(8), Validators.maxLength(11)])
    });
  }

  almacenFijo(almacen){
    console.log(almacen);
    this.storage.set('almacen', almacen);
  }

  loadAlmacenes(){
    this.restProvider.loadAlmacenes(this.user_id,this.permiso).subscribe(
      (res) => {
        console.log("Result del provider");
        console.log(res);
        this.success = res['auth'];
        this.almacenes = res['almacenes'];
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  setAlmacen(event){
    this.almacen = event;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchUbicacionPage');
  }

  ionViewWillEnter(){
    this.loadCodes();
  }

  searchRelaciones(){
    if(this.ubicacionform.valid){
      this.loadCodes();
    }else{
      let toast = this.toastCtrl.create({
        message: "Debe seleccionar el almacen y la ubicación debe tener el formato: ##.##.## o ##.##.##.##",
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
  }

  loadCodes(){
    this.restProvider.searchCodesRel(this.user_id,this.permiso,this.ubicacion.almacen,this.ubicacion.ubicacion).subscribe(
      (res) => {
        console.log("Entro a la carga de los codigos de las relaciones");
        console.log(res);
        this.success = res['auth'];
        if(this.success=="yes"){
            this.codigos = res['codigos'];
            console.log(" lista de Codigos");
            console.log(this.codigos);
        }
        //this.total = res['total'];
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  modificar(codigo){
    this.navCtrl.push(RelacionesModifyPage,{codigo:codigo});
  }

  eliminar(codigo){
    this.restProvider.deleteRelacion(this.user_id,this.permiso,codigo).subscribe(
      (res) => {
        console.log("Result del provider");
        console.log(res);
        this.success = res['auth'];
        let toast = this.toastCtrl.create({
          message: "Se ha eliminado la relacion correctamente.",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
        let index: number = this.codigos.indexOf(codigo); // <-- todo?
        if(index > -1){
            this.codigos.splice(index, 1);
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

}
