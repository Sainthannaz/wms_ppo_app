import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchUbicacionPage } from './search-ubicacion';

@NgModule({
  declarations: [
    SearchUbicacionPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchUbicacionPage),
  ],
})
export class SearchUbicacionPageModule {}
