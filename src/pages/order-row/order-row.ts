import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { RelacionModalPage } from '../relacion-modal/relacion-modal';

@IonicPage()
@Component({
  selector: 'page-order-row',
  templateUrl: 'order-row.html',
})
export class OrderRowPage {

  order:any[];
  usuario:any;
  codigo:any;
  success:any;
  user_id:any;
  permiso:any;
  detalle:any[];
  lotes:any[];
  ubicaciones:any[];
  row = {
    'lote': '',
    'ubicacion': '',
    'piezas': '0',
    'cajas': '0',
    'resto':'0',
    'reempaque': '',
    'total': '',
    'comentario': '',
    'eliminar': false
  }
  showFields = false;
  showAddRelation = false;
  eliminar = 0;
  porcentaje = 0;
  pendiente = 0;
  loteSelected = "";
  ubicacionSelected = "";

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage:Storage, public restProvider:RestProvider, public toastCtrl: ToastController,
    public modalCtrl:ModalController) {
    this.order = navParams.data.orderRow;
    this.usuario = navParams.data.usuario;
    this.detalle = navParams.data.detalle;
    this.codigo = navParams.data.detalle.codigo;
    this.loteSelected = navParams.data.detalle.lote;
    this.ubicacionSelected = navParams.data.detalle.ubicacion;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderRowPage');
  }

  ionViewWillEnter(){
    Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
      this.user_id = values[0];
      this.permiso = values[1];
      this.loadCodigoLotes();
      this.pendiente = this.order[0].cantidad - this.order[0].total1;
    });
  }

  loadCodigoLotes(){
    this.restProvider.loadCodigoLotes(this.user_id,this.permiso,this.codigo).subscribe(
      (res) => {
        this.success = res['auth'];
        if(this.success=="yes"){
          this.lotes = res['lotes'];
          if(this.lotes.length>0){
            this.showAddRelation = true;
            this.row.lote = this.loteSelected;
            this.row.ubicacion = this.ubicacionSelected;
          }
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  searchUbicacion(event){
    this.loadLotesUbicacion(event);
  }

  loadLotesUbicacion(lote){
    this.restProvider.loadLotesUbicacion(this.user_id,this.permiso,this.codigo,lote).subscribe(
      (res) => {
        this.success = res['auth'];
        if(this.success=="yes"){
          this.ubicaciones = res['ubicaciones'];
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  enableFields(event){
    if(event!=""){
      this.showFields = true;
    }
  }

  setEliminar(event){
    if(this.row.eliminar){
      this.eliminar = 1;
    }else{
      this.eliminar = 0;
    }
  }

  salir(){
    this.navCtrl.pop();
  }

  sendData(){
    if(this.row.piezas=="")
      this.row.piezas = "0";
    if(this.row.cajas=="")
      this.row.cajas = "0";
    if(this.row.resto=="")
      this.row.resto = "0";
    if(this.pendiente>0){
      let total = parseInt(this.row.piezas) * parseInt(this.row.cajas) + parseInt(this.row.resto);

      if(total<=this.pendiente && total!=0){
        this.restProvider.sendRowData(this.user_id,this.permiso,this.order,this.row,this.eliminar,this.usuario).subscribe(
          (res) => {
            this.success = res['auth'];
            this.order = res['order'];
            this.porcentaje = res['porcentaje'];
            this.pendiente = this.order[0].cantidad - this.order[0].total1;
            this.row.piezas = '';
            this.row.cajas = '';
            this.row.resto = '';
            this.row.reempaque = '';
            this.eliminar = 0;
            this.row.total = '';
            this.row.lote = '';
            this.row.ubicacion = '';
            this.showFields = false;
            let toast = this.toastCtrl.create({
              message: "Se ha registrado correctamente.",
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
            if(this.pendiente==0){
              this.navCtrl.pop();
            }
          },
          (error) =>{
            console.error(error);
          }
        )
      }else{
        let toast = this.toastCtrl.create({
          message: "El total de productos no puede ser cero o mayor a la cantidad pendiente",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }
    }else{
      let toast = this.toastCtrl.create({
        message: "Ya se ha completado la cantidad solicitada.",
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }

  }

  addRelacion(){
    let myModal = this.modalCtrl.create(RelacionModalPage,{order:this.order});
    myModal.onDidDismiss(data => {
      this.lotes = data;
      if(this.lotes.length>0){
        this.showAddRelation = true;
      }
    });
    myModal.present();
  }

}
