import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderRowPage } from './order-row';

@NgModule({
  declarations: [
    OrderRowPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderRowPage),
  ],
})
export class OrderRowPageModule {}
