import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { UbicacionDetailPage } from '../ubicacion-detail/ubicacion-detail';
import { UbicacionModifyPage } from '../ubicacion-modify/ubicacion-modify';



@IonicPage()
@Component({
  selector: 'page-ubicacion',
  templateUrl: 'ubicacion.html',
})

export class UbicacionPage {

  user_id:any;
  permiso:any;
  pred_almacen: any;
  success:any;
  exists:any;
  almacenes:any[];
  ubicacion = {almacen:'',ubicacion:''}
  detail = {
    pasillo:'',
    modulo:'',
    nivel:'',
    alto:'',
    ancho:'',
    fondo:'',
    zona:'',
    picking:0,
    montacargas:0,
    piso:0,
    picking_val:'',
    montacargas_val:'',
    piso_val:'',
    seccion:''
  }
  ubicacionform: FormGroup;
  loader: any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider:RestProvider, private storage:Storage, public menuCtrl:MenuController,
    private alertCtrl:AlertController, public toastCtrl:ToastController, public loadingCtrl: LoadingController) {
      if(navParams.data.ubicacion)
        this.ubicacion = navParams.data.ubicacion;
      Promise.all([this.storage.get("id"), this.storage.get("permiso"), this.storage.get("almacen")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
        this.pred_almacen = values[2];
        this.loadAlmacenes();
      });
  }

  ngOnInit() {
    this.ubicacionform = new FormGroup({
      almacen: new FormControl('', [Validators.required]),
      ubicacion: new FormControl('', [Validators.required, Validators.pattern('[0-9]{2}[.]{1}[0-9]{2}[.]{1}[0-9]{2}([.]{1}[0-9]{2})?'), Validators.minLength(8), Validators.maxLength(11)])
    });
  }

  almacenFijo(almacen){
    console.log(almacen);
    this.storage.set('almacen', almacen);
  }

  loadAlmacenes(){
    this.showLoader('Cargando...');
    this.restProvider.loadAlmacenes(this.user_id,this.permiso).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          this.almacenes = res['almacenes'];
        }
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UbicacionPage');
    this.ubicacion.almacen = '';
    this.ubicacion.ubicacion = '';
  }

  existsUbicacion(){
    this.showLoader('¡Buscando Ubicación!');
    if(this.ubicacionform.valid){
      this.restProvider.existsAlmacen(this.user_id,this.permiso,this.ubicacion.almacen,this.ubicacion.ubicacion).subscribe(
        (res) => {
          console.log(res);
          this.success = res['auth'];
          this.hideLoader();
          if(this.success=="yes"){
              this.exists = res['exists'];
              if(this.exists=="yes"){
                console.log("Mostrar opciones para actualizar o eliminar");
                this.presentConfirm();
              }else{
                this.navCtrl.push(UbicacionDetailPage,{ubicacion:this.ubicacion});
                //this.ubicacion.almacen = ""
                //this.ubicacion.ubicacion = "";
              }
          }
          //this.total = res['total'];
        },
        (error) =>{
          console.error(error);
        }
      )
    }else{
      let toast = this.toastCtrl.create({
        message: "Debe seleccionar el almacen y la ubicación debe tener el formato: ##.##.## o ##.##.##.##",
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }

  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Ubicación existente',
      message: '¿Qué desea hacer?',
      buttons: [
        {
          text: 'Eliminar',
          handler: () => {
            console.log('Eliminar clicked');
            this.deleteUbicacion();
          }
        },
        {
          text: 'Modificar',
          handler: () => {
            console.log('Modificar clicked');
            this.getUbicacion();
          }
        }
      ]
    });
    alert.present();
  }

  deleteUbicacion(){
    this.showLoader('Borrando Ubicación!');
    this.restProvider.deleteUbicacion(this.user_id,this.permiso,this.ubicacion.almacen,this.ubicacion.ubicacion).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          this.ubicacionform.reset();
          let toast = this.toastCtrl.create({
            message: "Se ha eliminado la ubicación correctamente.",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        }else{
          let toast = this.toastCtrl.create({
            message: "Ha ocurrido un error al eliminar, intentelo nuevamente.",
            duration: 5000,
            position: 'bottom'
          });
          toast.present();
        }
        //this.total = res['total'];
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  getUbicacion(){
    this.showLoader('Cargando Ubicación!');
    this.restProvider.getUbicacion(this.user_id,this.permiso,this.ubicacion.almacen,this.ubicacion.ubicacion).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          this.detail.pasillo = res['result'][0].pasillo;
          this.detail.modulo = res['result'][0].modulo;
          this.detail.nivel = res['result'][0].nivel;
          this.detail.seccion = res['result'][0].seccion;
          this.detail.alto = res['result'][0].alto;
          this.detail.ancho = res['result'][0].ancho;
          this.detail.fondo = res['result'][0].fondo;
          this.detail.zona = res['result'][0].zona;
          this.detail.picking = parseInt(res['result'][0].picking);
          this.detail.picking_val = res['result'][0].picking;
          this.detail.montacargas = parseInt(res['result'][0].montacargas);
          this.detail.montacargas_val = res['result'][0].montacargas;
          this.detail.piso = parseInt(res['result'][0].piso);
          this.detail.piso_val = res['result'][0].piso;
          this.navCtrl.push(UbicacionModifyPage,{detail:this.detail,ubicacion:this.ubicacion});
          //this.ubicacion.almacen = "";
          //this.ubicacion.ubicacion = "";
          this.ubicacionform.reset();
        }else{
          let toast = this.toastCtrl.create({
            message: "Ha ocurrido un error, intentelo nuevamente.",
            duration: 5000,
            position: 'bottom'
          });
          toast.present();
        }
        //this.total = res['total'];
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  private showLoadingHandler(message) {
      if (this.loader == null) {
          this.loader = this.loadingCtrl.create({
              content: message
          });
          this.loader.present();
      } else {
          this.loader.data.content = message;
      }
  }

  private hideLoadingHandler() {
      if (this.loader != null) {
          this.loader.dismiss();
          this.loader = null;
      }
  }

  public showLoader(message) {
      this.showLoadingHandler(message);
  }

  public hideLoader() {
      this.hideLoadingHandler();
  }
}
