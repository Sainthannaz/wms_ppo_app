import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-order-update',
  templateUrl: 'order-update.html',
})
export class OrderUpdatePage {

  user_id:any;
  permiso:any;
  order = {
    almacen:'',
    usuario1:'',
    usuario2:'',
    identificador:''
  }
  users:any[];
  success:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage:Storage, public restProvider:RestProvider, public toastCtrl:ToastController) {
      this.order = navParams.data.order;
      Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
        this.loadUsers();
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderUpdatePage');
  }

  loadUsers(){
    this.restProvider.loadUsersConteo(this.user_id,this.permiso,this.order.almacen).subscribe(
      (res) => {
        this.success = res['auth'];
        this.users = res['users'];
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  updateOrder(){
    console.log("Actualizar datos de la orden: ");
    console.log(this.order);
    if(this.order.usuario1!="" && this.order.usuario2!=""){
        this.addUsers();
    }else{
      let toast = this.toastCtrl.create({
        message: "Se deben asignar 2 usuarios al menos.",
        duration: 5000,
        position: 'bottom'
      });
      toast.present();
    }

  }

  addUsers(){
    this.restProvider.modifyOrder(this.user_id,this.permiso,this.order).subscribe(
      (res) => {
        console.log("Result de modify order");
        console.log(res);
        this.success = res['auth'];
        if(this.success=="yes"){
          this.navCtrl.pop();
        }else{
          let toast = this.toastCtrl.create({
            message: "Ocurrio un error al actualizar el conteo, intentelo nuevamente.",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

}
