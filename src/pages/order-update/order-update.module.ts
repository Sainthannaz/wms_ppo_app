import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderUpdatePage } from './order-update';

@NgModule({
  declarations: [
    OrderUpdatePage,
  ],
  imports: [
    IonicPageModule.forChild(OrderUpdatePage),
  ],
})
export class OrderUpdatePageModule {}
