import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { OrderListPage } from '../order-list/order-list';


@IonicPage()
@Component({
  selector: 'page-order-detail',
  templateUrl: 'order-detail.html',
})
export class OrderDetailPage {

  user_id:any;
  permiso:any;
  order:any[];
  usuario:any;
  success:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage:Storage, public restProvider:RestProvider,public toastCtrl: ToastController) {
      this.order = navParams.data.order;
      this.usuario = navParams.data.usuario;
      console.log("usuario: "+this.usuario);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderDetailPage');
  }

  ionViewWillEnter(){
    Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
      this.user_id = values[0];
      this.permiso = values[1];
    });
  }

  loadDetalles(){
    console.log("Iniciar la orden");
    console.log(this.order);
    this.initOrder();
  }

  initOrder(){
    this.restProvider.initOrderUser(this.user_id,this.permiso,this.order,this.usuario).subscribe(
      (res) => {
        this.success = res['auth'];
        if(this.success=="yes"){
          this.navCtrl.push(OrderListPage,{order:this.order,usuario:this.usuario});
        }else{
          let toast = this.toastCtrl.create({
            message: "Ocurrio un error al guardar la fecha de inicio de la orden.",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
          this.navCtrl.pop();
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

}
