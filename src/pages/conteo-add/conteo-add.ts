import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-conteo-add',
  templateUrl: 'conteo-add.html',
})



export class ConteoAddPage {

  detalle:any[];
  user_id:any;
  permiso:any;
  usuario:any;
  conteo = {
    piezasxcaja : 0,
    cajas : 0,
    resto : 0,
    total: 0,
    comentario: ""
  }


  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage:Storage, public restProvider:RestProvider, public alertCtrl:AlertController) {

    this.detalle = navParams.data.detalle;
    this.usuario = navParams.data.usuario;
    console.log(this.detalle);
    Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
      this.user_id = values[0];
      this.permiso = values[1];
      this.initConteo();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConteoAddPage');
  }

  presentAlert(subtitle) {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: subtitle,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  backTo(){
    this.navCtrl.pop();
  }

  initConteo(){
    this.restProvider.initConteo(this.user_id,this.permiso,this.detalle,this.usuario).subscribe(
      (res) => {
        console.log(res);
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  saveData(){
    if(this.conteo.piezasxcaja==0){
      this.presentAlert("Especifique las piezas x caja");
    }else{
      if(this.conteo.cajas==0){
        this.presentAlert("Especifique las cajas");
      }else{
        if(this.conteo.resto==0){
          this.presentAlert("Especifique el resto");
        }else{
          let total = this.conteo.piezasxcaja * this.conteo.cajas + this.conteo.resto;
          this.updateConteo();
//          this.navCtrl.pop();
        }
      }
    }
  }

  updateConteo(){
    this.restProvider.updateConteo(this.user_id,this.permiso,this.detalle,this.conteo,this.usuario).subscribe(
      (res) => {
        console.log(res);
        this.navCtrl.pop();
      },
      (error) =>{
        console.error(error);
      }
    )
  }

}
