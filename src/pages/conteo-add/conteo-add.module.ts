import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConteoAddPage } from './conteo-add';

@NgModule({
  declarations: [
    ConteoAddPage,
  ],
  imports: [
    IonicPageModule.forChild(ConteoAddPage),
  ],
})
export class ConteoAddPageModule {}
