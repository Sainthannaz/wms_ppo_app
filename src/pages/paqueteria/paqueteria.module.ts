import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaqueteriaPage } from './paqueteria';

@NgModule({
  declarations: [
    PaqueteriaPage,
  ],
  imports: [
    IonicPageModule.forChild(PaqueteriaPage),
  ],
})
export class PaqueteriaPageModule {}
