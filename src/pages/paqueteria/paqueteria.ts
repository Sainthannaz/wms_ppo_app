import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { PaqueteriaDetailPage } from '../paqueteria-detail/paqueteria-detail';
import { PaqueteriaModifyPage } from '../paqueteria-modify/paqueteria-modify';

@IonicPage()
@Component({
  selector: 'page-paqueteria',
  templateUrl: 'paqueteria.html',
})
export class PaqueteriaPage {

  user_id:any;
  permiso:any;
  sucursales:any[];
  paqueterias:any[];
  data:any[];
  success:any;
  sucursal:any;
  loader: any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider:RestProvider, private storage:Storage, public toastCtrl:ToastController, public loadingCtrl: LoadingController) {
      Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
        this.loadSucursales();
        this.loadPaqueterias();
      });
  }

  loadSucursales(){
    this.showLoader('Cargando...');
    this.restProvider.loadSucursales(this.user_id,this.permiso).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          this.sucursales = res['sucursales'];
        }
      },
      (error) =>{
        this.hideLoader();
        console.error(error);
      }
    )
  }

  loadPaqueterias(){
    this.showLoader('Cargando...');
    this.restProvider.loadPaqueterias(this.user_id,this.permiso).subscribe(
      (res) => {
        console.log(res);
        this.hideLoader();
        this.success = res['auth'];        
        if(this.success=="yes"){
          this.paqueterias = res['paqueterias'];
          this.data = res['paqueterias'];
        }
      },
      (error) =>{
        this.hideLoader();
        console.error(error);
      }
    )
  }

  filterItems(value){
    this.paqueterias = this.data;
    this.paqueterias = this.paqueterias.filter(item => item.sucursal === value);

  }

  addPaqueteria(){
    if(this.sucursal==null){
      let toast = this.toastCtrl.create({
        message: "Debe seleccionar la sucursal a la que agregará la paquetería.",
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }else{
      this.navCtrl.push(PaqueteriaDetailPage,{sucursal:this.sucursal});
    }
  }

  modifyPaqueteria(paqueteria){
    console.log("Modificar");
    this.navCtrl.push(PaqueteriaModifyPage,{paqueteria:paqueteria});
  }

  deletePaqueteria(paqueteria){
    console.log("Eliminar");
    this.showLoader('¡Borrando paqueteria, por favor espere!');
    this.restProvider.deletePaqueteria(this.user_id,this.permiso,paqueteria.id).subscribe(
      (res) => {
        console.log(res);
        this.hideLoader();
        this.success = res['auth'];
        if(this.success=="yes"){
          let toast = this.toastCtrl.create({
            message: "Se ha eliminado la paqueteria correctamente.",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
          this.sucursales = [];
          this.loadSucursales();
          this.loadPaqueterias();
        }else{
          let toast = this.toastCtrl.create({
            message: "Ha ocurrido un error al intentar eliminar, intentelo nuevamente.",
            duration: 5000,
            position: 'bottom'
          });
          toast.present();
        }
      },
      (error) =>{
        this.hideLoader();
        console.error(error);
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaqueteriaPage');
  }

  ionViewWillEnter(){
    this.sucursales = [];
    this.loadSucursales();
    this.loadPaqueterias();
  }

  private showLoadingHandler(message) {
      if (this.loader == null) {
          this.loader = this.loadingCtrl.create({
              content: message
          });
          this.loader.present();
      } else {
          this.loader.data.content = message;
      }
  }

  private hideLoadingHandler() {
      if (this.loader != null) {
          this.loader.dismiss();
          this.loader = null;
      }
  }

  public showLoader(message) {
      this.showLoadingHandler(message);
  }

  public hideLoader() {
      this.hideLoadingHandler();
  }
}
