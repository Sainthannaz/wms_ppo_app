import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LotePage } from './lote';

@NgModule({
  declarations: [
    LotePage,
  ],
  imports: [
    IonicPageModule.forChild(LotePage),
  ],
})
export class LotePageModule {}
