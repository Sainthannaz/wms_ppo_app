import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { LoteDetailPage } from '../lote-detail/lote-detail';
import { LoteModifyPage } from '../lote-modify/lote-modify';

@IonicPage()
@Component({
  selector: 'page-lote',
  templateUrl: 'lote.html',
})
export class LotePage {

  user_id:any;
  permiso:any;
  searchCode: string = '';
  searchControl: FormControl;
  searching: any = false;
  codes: any[];
  success:any;
  exists:any;
  showSearchCode:any = true;
  lote = {codigo:'',lote:''};
  detail = {
    alto:'',
    ancho:'',
    largo:'',
    peso:'',
    master:'',
    inner:'',
    ano:'',
    fecha:''
  }
  loader: any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider:RestProvider, private storage:Storage, public alertCtrl:AlertController,
    public toastCtrl:ToastController, public loadingCtrl: LoadingController) {
      this.searchControl = new FormControl();
      this.showSearchCode = true;
      Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
      });
  }

  onSearchInput(){
      this.searching = true;
  }

  setFilteredItems() {
    this.restProvider.loadSearchCode(this.user_id,this.permiso,this.searchCode).subscribe(
      (res) => {
        console.log("Result del provider");
        console.log(res);
        this.success = res['auth'];
        this.codes = res['result'];
      },
      (error) =>{
        console.error(error);
      }
    )
    //this.items = this.dataService.filterItems(this.searchTerm);
  }

  codeSelected(code){
    console.log(code);
    this.showSearchCode = false;
    this.lote.codigo = code.codigo;
    this.codes = [];
  }

  updateField(){
    this.lote.lote = this.lote.lote.toUpperCase();
  }

  existsLote(){
    this.showLoader('Verificando Lote, por favor espere!');
    this.restProvider.existsLote(this.user_id,this.permiso,this.lote.codigo,this.lote.lote).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
            this.exists = res['exists'];
            if(this.exists=="yes"){
              this.presentConfirm();
            }else{
              this.navCtrl.push(LoteDetailPage,{lote:this.lote});
              //this.lote.codigo = "";
              //this.lote.lote = "";
            }
        }
        //this.total = res['total'];
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Lote existente',
      message: '¿Qué desea hacer?',
      buttons: [
        {
          text: 'Eliminar',
          handler: () => {
            this.deleteLote();
          }
        },
        {
          text: 'Modificar',
          handler: () => {
            this.getLote();
          }
        }
      ]
    });
    alert.present();
  }

  deleteLote(){
    this.showLoader('¡Borrando Lote, por favor espere!');
    this.restProvider.deleteLote(this.user_id,this.permiso,this.lote.codigo,this.lote.lote).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          let toast = this.toastCtrl.create({
            message: "Se ha eliminado el lote correctamente.",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        }else{
          let toast = this.toastCtrl.create({
            message: "Ha ocurrido un error al eliminar, intentelo nuevamente.",
            duration: 5000,
            position: 'bottom'
          });
          toast.present();
        }
        //this.total = res['total'];
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  getLote(){
    this.showLoader('Obteniendo Lote, por favor espere!');
    this.restProvider.getLote(this.user_id,this.permiso,this.lote.codigo,this.lote.lote).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          this.detail.alto = res['result'][0].alto;
          this.detail.ancho = res['result'][0].ancho;
          this.detail.largo = res['result'][0].largo;
          this.detail.peso = res['result'][0].peso;
          this.detail.ano = res['result'][0].anio;
          this.detail.fecha = res['result'][0].fecha;
          this.detail.master = res['result'][0].master;
          this.detail.inner = res['result'][0].inner;
          this.navCtrl.push(LoteModifyPage,{detail:this.detail,lote:this.lote});
          //this.lote.codigo = "";
          //this.lote.lote = "";
        }else{
          let toast = this.toastCtrl.create({
            message: "Ha ocurrido un error, intentelo nuevamente.",
            duration: 5000,
            position: 'bottom'
          });
          toast.present();
        }
        //this.total = res['total'];
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LotePage');
    this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
      this.searching = false;
      this.setFilteredItems();
    });

    this.searchCode = '';
  }

  private showLoadingHandler(message) {
    if (this.loader == null) {
        this.loader = this.loadingCtrl.create({
            content: message
        });
        this.loader.present();
    } else {
        this.loader.data.content = message;
    }
  }

  private hideLoadingHandler() {
    if (this.loader != null) {
        this.loader.dismiss();
        this.loader = null;
    }
  }

  public showLoader(message) {
     this.showLoadingHandler(message);
  }

  public hideLoader() {
     this.hideLoadingHandler();
  }
}
