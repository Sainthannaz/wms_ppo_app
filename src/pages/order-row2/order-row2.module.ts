import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderRow2Page } from './order-row2';

@NgModule({
  declarations: [
    OrderRow2Page,
  ],
  imports: [
    IonicPageModule.forChild(OrderRow2Page),
  ],
})
export class OrderRow2PageModule {}
