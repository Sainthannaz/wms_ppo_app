import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-order-row2',
  templateUrl: 'order-row2.html',
})
export class OrderRow2Page {

  order:any[];
  usuario:any;
  codigo:any;
  success:any;
  user_id:any;
  permiso:any;
  detalle:any[];
  row = {
    'piezas': '0',
    'cajas': '0',
    'resto':'0',
    'reempaque': '',
    'total': '',
    'comentario': '',
  }
  showFields = false;
  porcentaje = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage:Storage, public restProvider:RestProvider, public toastCtrl: ToastController) {
      this.order = navParams.data.orderRow;
      this.usuario = navParams.data.usuario;
      this.detalle = navParams.data.detalle;
      this.codigo = navParams.data.detalle.codigo;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderRow2Page');
  }

  ionViewWillEnter(){
    Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
      this.user_id = values[0];
      this.permiso = values[1];
    });
  }

  salir(){
    this.navCtrl.pop();
  }

  sendData(){
    if(this.row.piezas=="")
      this.row.piezas = "0";
    if(this.row.cajas=="")
      this.row.cajas = "0";
    if(this.row.resto=="")
      this.row.resto = "0";

      let total = parseInt(this.row.piezas) * parseInt(this.row.cajas) + parseInt(this.row.resto);
      console.log("this.order[0].cantidad");
      console.log(this.order[0].cantidad);
      console.log("total");
      console.log(total);
      if(total==parseInt(this.order[0].cantidad)){
        this.restProvider.sendRow2Data(this.user_id,this.permiso,this.order,this.row,this.usuario).subscribe(
          (res) => {
            this.success = res['auth'];
            this.order = res['order'];
            this.porcentaje = res['porcentaje'];
            this.row.piezas = '';
            this.row.cajas = '';
            this.row.resto = '';
            this.row.reempaque = '';
            this.row.total = '';
            this.showFields = false;
            let toast = this.toastCtrl.create({
              message: "Se ha registrado correctamente.",
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
            this.navCtrl.pop();
          },
          (error) =>{
            console.error(error);
          }
        )
      }else{
        let toast = this.toastCtrl.create({
          message: "El total de productos no puede ser menor a la cantidad solicitada",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }


  }

}
