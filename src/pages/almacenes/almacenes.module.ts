import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlmacenesPage } from './almacenes';

@NgModule({
  declarations: [
    AlmacenesPage,
  ],
  imports: [
    IonicPageModule.forChild(AlmacenesPage),
  ],
})
export class AlmacenesPageModule {}
