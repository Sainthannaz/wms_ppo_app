import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { File } from '@ionic-native/file';

@IonicPage()
@Component({
  selector: 'page-codigos',
  templateUrl: 'codigos.html',
})
export class CodigosPage {

  csvFile: any;
  user_id:any;
  permiso:any;
  data:any;
  success:any;
  result = {
    filas:0,
    insertados:0,
    actualizados:0,
    errores:[]
  }
  loader: any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private file: File, public toastCtrl:ToastController,
    public restProvider:RestProvider, private storage:Storage, public loadingCtrl: LoadingController) {
      Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
      });
  }

  changeListener($event): void {
    this.file = $event.target.files[0];
    this.uploadFile();
  }

  uploadFile(){
    this.showLoader('¡Subiendo información, por favor espere!');
    this.restProvider.uploadCodigos(this.user_id,this.permiso,this.file).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          this.result.filas = res['filas'];
          this.result.insertados = res['insertados'];
          this.result.actualizados = res['actualizados'];
          this.result.errores = res['errores'];
          let toast = this.toastCtrl.create({
            message: "Se han registrado los códigos correctamente. ",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        }else{
          this.hideLoader();
          let toast = this.toastCtrl.create({
            message: "Ocurrio un error al guardar la información, intentelo nuevamente.",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CodigosPage');
  }

  private showLoadingHandler(message) {
    if (this.loader == null) {
        this.loader = this.loadingCtrl.create({
            content: message
        });
        this.loader.present();
    } else {
        this.loader.data.content = message;
    }
  }

  private hideLoadingHandler() {
    if (this.loader != null) {
        this.loader.dismiss();
        this.loader = null;
    }
  }

  public showLoader(message) {
     this.showLoadingHandler(message);
  }

  public hideLoader() {
     this.hideLoadingHandler();
  }

}
