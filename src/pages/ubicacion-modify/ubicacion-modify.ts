import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { DashboardPage } from '../dashboard/dashboard';

@IonicPage()
@Component({
  selector: 'page-ubicacion-modify',
  templateUrl: 'ubicacion-modify.html',
})
export class UbicacionModifyPage {

  ubicacion:any[];
  user_id:any;
  permiso:any;
  success:any;
  detail = {
    pasillo:'',
    modulo:'',
    nivel:'',
    alto:'',
    ancho:'',
    fondo:'',
    zona:'',
    picking:0,
    montacargas:0,
    piso:0,
    picking_val:'',
    montacargas_val:'',
    piso_val:'',
    seccion:''
  }
  loader: any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider:RestProvider, private storage:Storage, public toastCtrl:ToastController, 
    public loadingCtrl: LoadingController) {    
    console.log(navParams.data);
    console.log('Detalles ubicacion:');
    this.ubicacion = navParams.data.ubicacion;
    console.log(this.ubicacion);
    console.log('Detalles contenido:');
    this.detail = navParams.data.detail;
    console.log(this.detail);
    Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
      this.user_id = values[0];
      this.permiso = values[1];
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UbicacionModifyPage');
  }

  updateUbicacion(){
    console.log('Se actualizara la ubicación');
    console.log(this.detail);
    this.showLoader('Actualizando, por favor espere!');
    this.restProvider.updateUbicacion(this.user_id,this.permiso,this.ubicacion,this.detail).subscribe(
      (res) => {
        console.log('Resultado de la respuesta de actualizar...');
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          if(res['result']!=0){
            let toast = this.toastCtrl.create({
    	        message: "Se ha actualizado la ubicación correctamente.",
    	        duration: 3000,
    	        position: 'bottom'
    	      });
    	      toast.present();
            this.navCtrl.popTo(DashboardPage);
          }else{
            let toast = this.toastCtrl.create({
    	        message: "Ocurrio un error al actualizar la información, intentelo nuevamente.",
    	        duration: 3000,
    	        position: 'bottom'
    	      });
    	      toast.present();
          }
        }
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  datachanged(e:any,option){
    if(e.checked){
      if(option=="picking")
        this.detail.picking_val = '1'
      if(option=="montacargas")
        this.detail.montacargas_val = '1'
      if(option=="piso")
        this.detail.piso_val = '1'
    }else{
      if(option=="picking")
        this.detail.picking_val = '0'
      if(option=="montacargas")
        this.detail.montacargas_val = '0'
      if(option=="piso")
        this.detail.piso_val = '0'
    }
  }

  private showLoadingHandler(message) {
      if (this.loader == null) {
          this.loader = this.loadingCtrl.create({
              content: message
          });
          this.loader.present();
      } else {
          this.loader.data.content = message;
      }
  }

  private hideLoadingHandler() {
      if (this.loader != null) {
          this.loader.dismiss();
          this.loader = null;
      }
  }

  public showLoader(message) {
      this.showLoadingHandler(message);
  }

  public hideLoader() {
      this.hideLoadingHandler();
  }

  toUpperCaseZona(){
    console.log("Entro a upperCase Zona");
    this.detail.zona = this.detail.zona.toUpperCase();
  }
}
