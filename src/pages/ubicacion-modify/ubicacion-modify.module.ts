import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UbicacionModifyPage } from './ubicacion-modify';

@NgModule({
  declarations: [
    UbicacionModifyPage,
  ],
  imports: [
    IonicPageModule.forChild(UbicacionModifyPage),
  ],
})
export class UbicacionModifyPageModule {}
