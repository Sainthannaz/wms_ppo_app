import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConteoPage } from './conteo';

@NgModule({
  declarations: [
    ConteoPage,
  ],
  imports: [
    IonicPageModule.forChild(ConteoPage),
  ],
})
export class ConteoPageModule {}
