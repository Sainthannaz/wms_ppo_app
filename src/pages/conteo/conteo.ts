import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController, LoadingController, Content } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { UsersModalPage } from '../users-modal/users-modal';
import { FileConteoPage } from '../file-conteo/file-conteo';

@IonicPage()

@Component({
  selector: 'page-conteo',
  templateUrl: 'conteo.html', 
})

export class ConteoPage {
  @ViewChild(Content) content: Content;
  user_id:any;
  permiso:any;
  success:any;
  almacenes:any[];
  grupos:any[];
  clasificaciones:any[];
  codes:any[];
  conteo = {
    almacen:'',
    catin:false,
    opcion1:false,
    opcion2:false,
    opcion3:false,
    grupo:'',
    clasificacion:'',
    nombre:'',
    comentario:''
  }
  temporal = {
    catin:'0',
    opcion1:'0',
    opcion2:'0',
    opcion3:'0',
    grupo:'',
    clasificacion:''
  }
  codes_selected = new Array();
  users:any[];
  usuarios:any[];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider:RestProvider, private storage:Storage, public loadingCtrl: LoadingController,
    public toastCtrl:ToastController, public modalCtrl:ModalController) {
      Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
      
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConteoPage');
  }

  ionViewDidEnter(){
    let loading = this.loadingCtrl.create({content: 'Cargando, espere por favor...'});
    loading.present().then(() => {
      this.loadCodes();
      this.loadAlmacenes();
      this.loadGrupos();     
      loading.dismiss();
    });
     
  }

  loadAlmacenes(){
    this.restProvider.loadAlmacenes(this.user_id,this.permiso).subscribe(
      (res) => {
        this.success = res['auth'];
        this.almacenes = res['almacenes'];
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  loadGrupos(){
    this.restProvider.loadGrupos(this.user_id,this.permiso).subscribe(
      (res) => {
        this.success = res['auth'];
        this.grupos = res['grupos'];
        //this.grupos.push({grupo:'TODOS'});
        //console.log(this.grupos);
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  reset(){
    console.log('Reset');
    this.conteo.grupo = null;
  }

  loadClasificaciones(){
    this.restProvider.loadClasificaciones(this.user_id,this.permiso,this.conteo.grupo).subscribe(
      (res) => {
        this.success = res['auth'];
        this.clasificaciones = res['clasificaciones'];
        this.temporal.grupo = this.conteo.grupo;
        this.temporal.clasificacion = "";
        this.actualizarCodigos();
      },
      (error) =>{
        console.error(error);
      }
    )

  }

  loadCodes(){
    this.restProvider.loadCodes(this.user_id,this.permiso,this.temporal).subscribe(
      (res) => {
        this.success = res['auth'];
        this.codes = res['codes'];
        console.log(this.codes);
        for (let code of this.codes) {
          code['selected'] = '0';
          if(this.codes_selected.length>0){
            for(let item of this.codes_selected){
              if(item.codigo==code.codigo){
                code['selected'] = '1';
              }
            }
          }
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  loadUsers(){
    this.restProvider.loadUsersConteo(this.user_id,this.permiso,this.conteo.almacen).subscribe(
      (res) => {
        this.success = res['auth'];
        this.users = res['users'];
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  setStatusCode(code,event){
    if(event.checked){
      this.codes_selected.push(code);
    }else{
      this.codes_selected.forEach( (item, index) => {
       if(item.codigo === code.codigo) this.codes_selected.splice(index,1);
     });
    }
  }

  actualizarCodigos(){
    this.loadCodes();
    //this.codes_selected = [];
  }

  updateCatIn(event){
    if(event.checked){
      this.temporal.catin='1';
    }else{
      this.temporal.catin='0';
    }
    this.actualizarCodigos();
  }

  updateOpcion1(event){
    if(event.checked){
      this.temporal.opcion1='1';
    }else{
      this.temporal.opcion1='0';
    }
    this.actualizarCodigos();
  }

  updateOpcion2(event){
    if(event.checked){
      this.temporal.opcion2='1';
    }else{
      this.temporal.opcion2='0';
    }
    this.actualizarCodigos();
  }

  updateOpcion3(event){
    if(event.checked){
      this.temporal.opcion3='1';
    }else{
      this.temporal.opcion3='0';
    }
    this.actualizarCodigos();
  }

  updateClasificacion(){
    this.temporal.clasificacion = this.conteo.clasificacion;
    this.actualizarCodigos();
  }

  updateField(){
    this.conteo.nombre = this.conteo.nombre.toUpperCase();
  }

  updateField2(){
    this.conteo.comentario = this.conteo.comentario.toUpperCase();
  }

  addUsers(){
    let myModal = this.modalCtrl.create(UsersModalPage,{users:this.users});
    myModal.onDidDismiss(data => {
      this.usuarios = data;
    });
    myModal.present();
  }

  seleccionar(){
    console.log(this.codes);
    for (let code of this.codes) {
      code['selected'] = '1';
     
    }
  }

  limpiar() {
    console.log(this.codes);
    for (let code of this.codes) {
      code['selected'] = '0';
     
    }
  }

  toBottom(){
   this.content.scrollToBottom();
  }
  
  toTop(){
   this.content.scrollToTop();
  }

  addConteo(){
    if(this.codes_selected.length==0){
      let toast = this.toastCtrl.create({
        message: "No se han seleccionado códigos para el conteo.",
        duration: 5000,
        position: 'bottom',
        showCloseButton:true,
        closeButtonText: "Cerrar"
      });
      toast.present();
    }else{
      if(!this.usuarios){
        let toast = this.toastCtrl.create({
          message: "No se han agregado usuarios para el conteo.",
          duration: 5000,
          position: 'bottom',
          showCloseButton:true,
          closeButtonText: "Cerrar"
        });
        toast.present();
      }else{

        this.restProvider.addConteo(this.user_id,this.permiso,this.usuarios,this.codes_selected,this.conteo).subscribe(
          (res) => {
            let toast = this.toastCtrl.create({
              message: "Se han agregado los codigos para el conteo correctamente",
              duration: 5000,
              position: 'bottom',
              showCloseButton:true,
              closeButtonText: "Cerrar"
            });
            toast.present();
            //this.navCtrl.push(FileConteoPage,{identificador:res['identificador']});
            this.navCtrl.pop();
          },
          (error) =>{
            console.error(error);
          }
        )
      }
    }
  }

}
