import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConteoDetailPage } from './conteo-detail';

@NgModule({
  declarations: [
    ConteoDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ConteoDetailPage),
  ],
})
export class ConteoDetailPageModule {}
