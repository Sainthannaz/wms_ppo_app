import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { ConteoAddPage } from '../conteo-add/conteo-add';

@IonicPage()
@Component({
  selector: 'page-conteo-detail',
  templateUrl: 'conteo-detail.html',
})
export class ConteoDetailPage {

  user_id:any;
  permiso:any;
  detalles:any[];
  success:any;
  identificador: any;
  nombre:any;
  comentario:any;
  usuario:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage:Storage, public restProvider:RestProvider) {
    this.identificador = navParams.data.conteo.identificador;
    this.nombre = navParams.data.conteo.documento;
    this.comentario = navParams.data.conteo.comentario_in;
    this.usuario = navParams.data.usuario;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConteoDetailPage');
  }

  ionViewWillEnter(){
    Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
      this.user_id = values[0];
      this.permiso = values[1];
      this.loadDetalles();
    });
  }

  loadDetalles(){
    this.restProvider.loadConteoDetalles(this.user_id,this.permiso,this.identificador,this.usuario).subscribe(
      (res) => {
        console.log("Detalles");
        console.log(res);
        this.success = res['auth'];
        this.detalles = res['detalles'];
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  agregarConteo(detalle){
    this.navCtrl.push(ConteoAddPage,{detalle:detalle,usuario:this.usuario});
  }

}
