import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UbicacionDetailPage } from './ubicacion-detail';

@NgModule({
  declarations: [
    UbicacionDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(UbicacionDetailPage),
  ],
})
export class UbicacionDetailPageModule {}
