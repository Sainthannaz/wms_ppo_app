import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { DashboardPage } from '../dashboard/dashboard';

@IonicPage()
@Component({
  selector: 'page-ubicacion-detail',
  templateUrl: 'ubicacion-detail.html',
})
export class UbicacionDetailPage {

  ubicacion = {almacen:'',ubicacion:''}
  user_id:any;
  permiso:any;
  success:any;
  detail = {
    pasillo:'',
    modulo:'',
    nivel:'',
    alto:'',
    ancho:'',
    fondo:'',
    zona:'',
    picking:false,
    montacargas:false,
    piso:false,
    picking_val:'',
    montacargas_val:'',
    piso_val:'',
    seccion:''
  }
  temp_array:any[];
  loader: any = null;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider:RestProvider, private storage:Storage, public toastCtrl:ToastController, public loadingCtrl: LoadingController) {
    this.ubicacion = navParams.data.ubicacion;
    this.temp_array = this.ubicacion.ubicacion.split('.');
    console.log(this.temp_array);
    this.detail.pasillo = this.temp_array[0];
    this.detail.modulo = this.temp_array[1];
    this.detail.nivel = this.temp_array[2];
    if(this.temp_array.length==4){
      this.detail.seccion = this.temp_array[3];
    }
    Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
      this.user_id = values[0];
      this.permiso = values[1];
    });
    console.log("Dentro de ubicacion detail");
    console.log(this.ubicacion);
    console.log(this.detail);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UbicacionDetailPage');
  }

  insertUbicacion(){
    this.showLoader('¡Guardando, por favor espere!');
    this.restProvider.insertUbicacion(this.user_id,this.permiso,this.ubicacion,this.detail).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          if(res['result']!=0){
            let toast = this.toastCtrl.create({
    	        message: "Se ha registrado la ubicación correctamente. Registro: "+res['result'],
    	        duration: 3000,
    	        position: 'bottom'
    	      });
    	      toast.present();
            this.navCtrl.popTo(DashboardPage);
          }else{
            let toast = this.toastCtrl.create({
    	        message: "Ocurrio un error al guardar la información, intentelo nuevamente.",
    	        duration: 3000,
    	        position: 'bottom'
    	      });
    	      toast.present();
          }
        }
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )

  }

  datachanged(e:any,option){
    if(e.checked){
      if(option=="picking")
        this.detail.picking_val = '1'
      if(option=="montacargas")
        this.detail.montacargas_val = '1'
      if(option=="piso")
        this.detail.piso_val = '1'
    }else{
      if(option=="picking")
        this.detail.picking_val = '0'
      if(option=="montacargas")
        this.detail.montacargas_val = '0'
      if(option=="piso")
        this.detail.piso_val = '0'
    }
  }

  private showLoadingHandler(message) {
    if (this.loader == null) {
        this.loader = this.loadingCtrl.create({
            content: message
        });
        this.loader.present();
    } else {
        this.loader.data.content = message;
    }
  }

  private hideLoadingHandler() {
      if (this.loader != null) {
          this.loader.dismiss();
          this.loader = null;
      }
  }

  public showLoader(message) {
      this.showLoadingHandler(message);
  }

  public hideLoader() {
      this.hideLoadingHandler();
  }

  toUpperCaseZona(){
    console.log("Entro a upperCase Zona");
    this.detail.zona = this.detail.zona.toUpperCase();
  }
}
