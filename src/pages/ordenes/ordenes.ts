import { Component, ViewChild  } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, ToastController, AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { ConteoPage } from '../conteo/conteo';
import { ConteoDetailPage } from '../conteo-detail/conteo-detail';
import { ConteoUpdatePage } from '../conteo-update/conteo-update';
import { OrderUpdatePage } from '../order-update/order-update';
import { OrderDetailPage } from '../order-detail/order-detail';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-ordenes',
  templateUrl: 'ordenes.html',
})
export class OrdenesPage {

	showToolbar: boolean = false;
	 @ViewChild(Content) content: Content;

   user_id:any;
   permiso:any;
   conteos:any[];
   conteos2:any[];
   conteos3:any[];
   conteos4:any[];
   success:any;
   usuario:any;
   message:any;
   email:any;
   orders:any[];
   orders2:any[];

   regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

  	constructor(public navCtrl: NavController, public navParams: NavParams, public translateService: TranslateService,
      private storage:Storage, public restProvider:RestProvider, public toastCtrl:ToastController,
      public alertCtrl: AlertController) {
  	}

  	ionViewDidLoad() {
    	console.log('ionViewDidLoad OrdenesPage');
  	}

    ionViewWillEnter(){
      Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
        if(this.permiso.toUpperCase()=="ADMIN"){
          this.loadConteos();
          this.loadOrders();
        }else{
          this.loadConteosUsuario1();
          this.loadOrdersUsuario1();
          //this.usuario = "usuario1";
        }
      });
    }

    loadConteos(){
      this.restProvider.loadConteos(this.user_id,this.permiso).subscribe(
        (res) => {
          this.success = res['auth'];
          this.conteos = res['conteos'];
          console.log(this.conteos);
          //this.conteos2 = res['conteos2'];
          //this.conteos3 = res['conteos3'];
          //this.conteos4 = res['conteos4'];
          this.setPorcentajeAdmin();
          //this.setPorcentaje2();
          //this.setPorcentaje3();
          //this.setPorcentaje4();
        },
        (error) =>{
          console.error(error);
        }
      )
    }

    loadOrders(){
      this.restProvider.loadOrders(this.user_id,this.permiso).subscribe(
        (res) => {
          this.success = res['auth'];
          this.orders = res['orders'];
          console.log(this.orders);
          //this.conteos2 = res['conteos2'];
          //this.conteos3 = res['conteos3'];
          //this.conteos4 = res['conteos4'];
          this.setPorcentajeOrderAdmin();
          //this.setPorcentaje2();
          //this.setPorcentaje3();
          //this.setPorcentaje4();
        },
        (error) =>{
          console.error(error);
        }
      )
    }

    setPorcentajeAdmin(){
      if(this.conteos.length>0){
        for(let contador of this.conteos){
          this.loadPorcentajeAdmin(contador);
        }
      }
    }

    setPorcentajeOrderAdmin(){
      if(this.orders.length>0){
        for(let contador of this.orders){
          this.loadPorcentajeAdmin(contador);
        }
      }
    }

    setPorcentaje(){
      if(this.conteos.length>0){
        for(let contador of this.conteos){
          this.loadPorcentaje(contador);
        }
      }
    }

    setPorcentaje2(){
      if(this.conteos2.length>0){
        for(let contador of this.conteos2){
          this.loadPorcentaje2(contador);
        }
      }
    }

    setPorcentajeOrder(){
      if(this.orders.length>0){
        for(let contador of this.orders){
          this.loadPorcentaje(contador);
        }
      }
    }

    setPorcentajeOrder2(){
      if(this.orders2.length>0){
        for(let contador of this.orders2){
          this.loadPorcentaje2(contador);
        }
      }
    }

    setPorcentaje3(){
      if(this.conteos3.length>0){
        for(let contador of this.conteos3){
          this.loadPorcentaje3(contador);
        }
      }
    }

    setPorcentaje4(){
      if(this.conteos4.length>0){
        for(let contador of this.conteos4){
          this.loadPorcentaje4(contador);
        }
      }
    }

    loadPorcentajeAdmin(contador){
      this.restProvider.getPorcentajeAvanceAdmin(this.user_id,this.permiso,contador.identificador).subscribe(
        (res) => {
          this.success = res['auth'];
          contador['porcentaje'] = res['porcentaje'];
        },
        (error) =>{
          console.error(error);
        }
      )
    }

    loadPorcentaje(contador){
      this.restProvider.getPorcentajeAvance(this.user_id,this.permiso,contador.identificador).subscribe(
        (res) => {
          this.success = res['auth'];
          contador['porcentaje'] = res['porcentaje'];
        },
        (error) =>{
          console.error(error);
        }
      )
    }

    loadPorcentaje2(contador){
      this.restProvider.getPorcentajeAvance2(this.user_id,this.permiso,contador.identificador).subscribe(
        (res) => {
          this.success = res['auth'];
          contador['porcentaje'] = res['porcentaje'];
        },
        (error) =>{
          console.error(error);
        }
      )
    }

    loadPorcentaje3(contador){
      this.restProvider.getPorcentajeAvance3(this.user_id,this.permiso,contador.identificador).subscribe(
        (res) => {
          this.success = res['auth'];
          contador['porcentaje'] = res['porcentaje'];
        },
        (error) =>{
          console.error(error);
        }
      )
    }

    loadPorcentaje4(contador){
      this.restProvider.getPorcentajeAvance4(this.user_id,this.permiso,contador.identificador).subscribe(
        (res) => {
          this.success = res['auth'];
          contador['porcentaje'] = res['porcentaje'];
        },
        (error) =>{
          console.error(error);
        }
      )
    }

    loadConteosUsuario1(){
      this.restProvider.loadConteosUsuario1(this.user_id,this.permiso).subscribe(
        (res) => {
          console.log(res);
          this.success = res['auth'];
          this.conteos = res['conteos'];
          this.conteos2 = res['conteos2'];
          this.conteos3 = res['conteos3'];
          this.conteos4 = res['conteos4'];
          console.log("¡Conteos!");
          console.log(this.conteos);
          console.log(this.conteos2);
          console.log(this.conteos3);
          console.log(this.conteos4);
          this.setPorcentaje();
          this.setPorcentaje2();
          this.setPorcentaje3();
          this.setPorcentaje4();
        },
        (error) =>{
          console.error(error);
        }
      )
    }

    loadOrdersUsuario1(){
      this.restProvider.loadOrdersUsuario1(this.user_id,this.permiso).subscribe(
        (res) => {
          console.log(res);
          this.success = res['auth'];
          console.log("¡Ordenes!");
          this.orders = res['orders'];
          this.orders2 = res['orders2'];
          console.log(this.orders);
          console.log(this.orders2);
          this.setPorcentajeOrder();
          this.setPorcentajeOrder2();
        },
        (error) =>{
          console.error(error);
        }
      )
    }

  	toggleToolbar() {
    	this.showToolbar = !this.showToolbar;
    	this.content.resize();
  	}

    reporte(){
      console.log("Entro a la funcion reporte");
    }

    conteo(){
      this.navCtrl.push(ConteoPage);
    }

    conteoDetail(conteo){
      if(this.permiso.toUpperCase()!="ADMIN"){
        this.navCtrl.push(ConteoDetailPage,{conteo:conteo,usuario:"usuario1"});
      }else{
        let toast = this.toastCtrl.create({
          message: "El administrador no puede realizar conteos",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }

    }

    orderDetail(order){
      console.log("Entrar al detalle de la orden");
      if(this.permiso.toUpperCase()!="ADMIN"){
        this.navCtrl.push(OrderDetailPage,{order:order,usuario:"usuario1"});
      }else{
        let toast = this.toastCtrl.create({
          message: "El administrador no puede realizar conteos",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }
    }

    conteoDetail2(conteo){
      if(this.permiso.toUpperCase()!="ADMIN"){
        this.navCtrl.push(ConteoDetailPage,{conteo:conteo,usuario:"usuario2"});
      }else{
        let toast = this.toastCtrl.create({
          message: "El administrador no puede realizar conteos",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }

    }

    orderDetail2(order){
      console.log("Entrar al detalle de la orden");
      if(this.permiso.toUpperCase()!="ADMIN"){
        this.navCtrl.push(OrderDetailPage,{order:order,usuario:"usuario2"});
      }else{
        let toast = this.toastCtrl.create({
          message: "El administrador no puede realizar conteos",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }
    }

    conteoDetail3(conteo){
      if(this.permiso.toUpperCase()!="ADMIN"){
        this.navCtrl.push(ConteoDetailPage,{conteo:conteo,usuario:"usuario3"});
      }else{
        let toast = this.toastCtrl.create({
          message: "El administrador no puede realizar conteos",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }

    }

    conteoDetail4(conteo){
      if(this.permiso.toUpperCase()!="ADMIN"){
        this.navCtrl.push(ConteoDetailPage,{conteo:conteo,usuario:"usuario4"});
      }else{
        let toast = this.toastCtrl.create({
          message: "El administrador no puede realizar conteos",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }

    }

    deleteConteo(contador){
      this.restProvider.deleteConteo(this.user_id,this.permiso,contador.identificador).subscribe(
        (res) => {
          this.success = res['auth'];
          this.message = res['message'];
          if(this.message!="ok"){
            let toast = this.toastCtrl.create({
              message: this.message,
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
          }else{
            this.loadConteos();
          }
        },
        (error) =>{
          console.error(error);
        }
      )
    }

    deleteOrder(contador){
      console.log("Entró al deleteOrder");
      this.restProvider.deleteOrder(this.user_id,this.permiso,contador.identificador).subscribe(
        (res) => {
          this.success = res['auth'];
          this.message = res['message'];
          if(this.message!="ok"){
            let toast = this.toastCtrl.create({
              message: this.message,
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
          }else{
            this.loadOrders();
          }
        },
        (error) =>{
          console.error(error);
        }
      )
    }

    updateConteo(conteo){
      this.navCtrl.push(ConteoUpdatePage,{conteo:conteo});
    }

    updateOrder(order){
      console.log("Entró al updateOrder");
      this.navCtrl.push(OrderUpdatePage,{order:order});
    }

    sendEmail(conteo){
      let alert = this.alertCtrl.create();
      alert.setTitle('Ingresar correo');

      alert.addInput({
        type: 'text',
        label: 'Correo electrónico',
        value: this.email
      });

      alert.addButton('Cancel');
      alert.addButton({
        text: 'OK',
        handler: data => {
          this.email = data[0];
          if(data[0]!="" && this.regexp.test(data[0])){
            this.restProvider.sendEmail(this.user_id,this.permiso,conteo.identificador,this.email).subscribe(
              (res) => {
                this.success = res['auth'];
                this.message = res['message'];
                if(this.message!="ok"){
                  let toast = this.toastCtrl.create({
                    message: this.message,
                    duration: 3000,
                    position: 'bottom'
                  });
                  toast.present();
                }else{
                  let toast = this.toastCtrl.create({
                    message: "El email se ha enviado correctamente",
                    duration: 3000,
                    position: 'bottom'
                  });
                  toast.present();
                }
              },
              (error) =>{
                console.error(error);
              }
            )
          }else{
            let toast = this.toastCtrl.create({
              message: "Email no valido",
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
          }
        }
      });
      alert.present();
    }

}
