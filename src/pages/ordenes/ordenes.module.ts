import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrdenesPage } from './ordenes';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    OrdenesPage,
  ],
  imports: [
    IonicPageModule.forChild(OrdenesPage),
    TranslateModule.forChild()
  ],
})
export class OrdenesPageModule {}
