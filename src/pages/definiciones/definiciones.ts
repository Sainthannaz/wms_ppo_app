import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController, Platform } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { UbicacionPage } from '../ubicacion/ubicacion';
import { LotePage } from '../lote/lote';
import { UsuariosPage, CodigosPage } from '../pages';
import { RelacionesPage } from '../relaciones/relaciones';
import { AlmacenesPage } from '../almacenes/almacenes';
import { PaqueteriaPage } from '../paqueteria/paqueteria';
import { Storage } from '@ionic/storage';
import { User } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-definiciones',
  templateUrl: 'definiciones.html',
})
export class DefinicionesPage {
  user_id:any;
  permiso:any;
  usuarios: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform,
    public translateService: TranslateService, public menu: MenuController,
    private alertCtrl: AlertController, private storage: Storage, private user: User) {
    Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
       this.user_id = values[0]; console.log(this.user_id);        
        this.permiso = values[1]; console.log(this.permiso);
        if (this.permiso == "admin" || this.permiso == "ADMIN"){
         this.usuarios = true;
        } else if (this.permiso == "user" || this.permiso == "USER"){
         this.usuarios = false;
        }
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DefinicionesPage');
  }

  openUbicacion(){
    this.navCtrl.push(UbicacionPage);
  }

  openLote(){
    this.navCtrl.push(LotePage);
  }

  openCodigos(){
    this.navCtrl.push(CodigosPage);
  }

  openUsuarios(){
    this.navCtrl.push(UsuariosPage);
  }

  openRelaciones(){
    this.navCtrl.push(RelacionesPage);
  }

  openAlmacenes(){
    this.navCtrl.push(AlmacenesPage);
  }

  openPaqueterias(){
    this.navCtrl.push(PaqueteriaPage);
  }

}
