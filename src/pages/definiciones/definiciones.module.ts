import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { DefinicionesPage } from './definiciones';

@NgModule({
  declarations: [
    DefinicionesPage,
  ],
  imports: [
    IonicPageModule.forChild(DefinicionesPage),
    TranslateModule.forChild()
  ],
})
export class DefinicionesPageModule {}
