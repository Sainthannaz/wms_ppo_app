import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-users-modal',
  templateUrl: 'users-modal.html',
})
export class UsersModalPage {

  users:any[];
  usuarios = {
    usuario1:'',
    usuario2:'',
    usuario3:'',
    usuario4:''
  }

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl:ViewController,public toastCtrl:ToastController) {
    this.users = this.navParams.get('users')
    console.log("En el modal");
    console.log(this.users);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UsersModalPage');
  }

  closeModal(){
    console.log("Usuarios");
    console.log(this.usuarios);
    if(this.usuarios.usuario1!="" && this.usuarios.usuario2!=""){
        this.viewCtrl.dismiss(this.usuarios);
    }else{
      let toast = this.toastCtrl.create({
        message: "Se deben asignar 2 usuarios al menos.",
        duration: 5000,
        position: 'bottom'
      });
      toast.present();
    }
  }

}
