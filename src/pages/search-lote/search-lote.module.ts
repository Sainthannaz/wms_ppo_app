import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchLotePage } from './search-lote';

@NgModule({
  declarations: [
    SearchLotePage,
  ],
  imports: [
    IonicPageModule.forChild(SearchLotePage),
  ],
})
export class SearchLotePageModule {}
