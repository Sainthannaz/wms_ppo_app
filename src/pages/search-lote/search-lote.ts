import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { LoteDetailPage } from '../lote-detail/lote-detail';
import { LoteModifyPage } from '../lote-modify/lote-modify';
/**
 * Generated class for the SearchLotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-lote',
  templateUrl: 'search-lote.html',
})
export class SearchLotePage {
  searchCode: string = '';
  searchControl: FormControl;
  searching: any = false;
  codes: any[];
  success:any;
  exists:any;
  user_id:any;
  permiso:any;
  showSearchCode:any = true;
  loader: any = null;
  lote = {codigo:'',lote:''};
  detail = {
    alto:'',
    ancho:'',
    largo:'',
    peso:'',
    master:'',
    inner:'',
    ano:'',
    fecha:''
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, public restProvider:RestProvider, private storage:Storage, public alertCtrl:AlertController,
    public toastCtrl:ToastController, public loadingCtrl: LoadingController) {
  	this.searchControl = new FormControl();
    this.showSearchCode = true;
    Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchCodigosPage');
    this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
      this.searching = false;
      this.setFilteredItems();
    });

    this.searchCode = '';
  }

  setFilteredItems() {
  	console.log('Vamos a buscar...');
  	console.log(this.searchCode);
  	this.restProvider.loadSearchCodeInLote(this.user_id,this.permiso,this.searchCode).subscribe(
      (res) => {
        console.log("Result del provider");
        console.log(res);
        this.success = res['auth'];
        this.codes = res['result'];
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  onSearchInput(){
    this.searching = true;
  }

  codeSelected(code){
    console.log(code);
    //this.showSearchCode = false;
    console.log(code.codigo);
    console.log(code.lote);
    this.lote.codigo = code.codigo;
    this.lote.lote = code.lote;
    this.showLoader('Obteniendo Lote, por favor espere!');
    this.restProvider.getLote(this.user_id,this.permiso,this.lote.codigo,this.lote.lote).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          this.detail.alto = res['result'][0].alto;
          this.detail.ancho = res['result'][0].ancho;
          this.detail.largo = res['result'][0].largo;
          this.detail.peso = res['result'][0].peso;
          this.detail.ano = res['result'][0].anio;
          this.detail.fecha = res['result'][0].fecha;
          this.detail.master = res['result'][0].master;
          this.detail.inner = res['result'][0].inner;
          this.navCtrl.push(LoteModifyPage,{detail:this.detail,lote:this.lote});
        }else{
          let toast = this.toastCtrl.create({
            message: "Ha ocurrido un error, intentelo nuevamente.",
            duration: 5000,
            position: 'bottom'
          });
          toast.present();
        }
        //this.total = res['total'];
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  eliminar(codigo){
    console.log(codigo);
     this.showLoader('¡Borrando Lote, por favor espere!');
    this.restProvider.deleteLote(this.user_id,this.permiso, codigo.codigo, codigo.lote).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          let toast = this.toastCtrl.create({
            message: "Se ha eliminado el lote correctamente.",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
          this.setFilteredItems();
        }else{
          let toast = this.toastCtrl.create({
            message: "Ha ocurrido un error al eliminar, intentelo nuevamente.",
            duration: 5000,
            position: 'bottom'
          });
          toast.present();
        }
        //this.total = res['total'];
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  

  private showLoadingHandler(message) {
    if (this.loader == null) {
        this.loader = this.loadingCtrl.create({
            content: message
        });
        this.loader.present();
    } else {
        this.loader.data.content = message;
    }
  }

  private hideLoadingHandler() {
    if (this.loader != null) {
        this.loader.dismiss();
        this.loader = null;
    }
  }

  public showLoader(message) {
     this.showLoadingHandler(message);
  }

  public hideLoader() {
     this.hideLoadingHandler();
  }

}
