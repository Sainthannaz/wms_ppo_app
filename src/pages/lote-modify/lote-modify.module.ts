import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoteModifyPage } from './lote-modify';

@NgModule({
  declarations: [
    LoteModifyPage,
  ],
  imports: [
    IonicPageModule.forChild(LoteModifyPage),
  ],
})
export class LoteModifyPageModule {}
