import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-lote-modify',
  templateUrl: 'lote-modify.html',
})
export class LoteModifyPage {

  user_id:any;
  permiso:any;
  success:any;
  lote = {codigo:'',lote:''};
  detail = {
    alto:'',
    ancho:'',
    largo:'',
    peso:'',
    master:'',
    inner:'',   
    ano:'',
    fecha:''
  }
  loader: any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider:RestProvider, private storage:Storage, public toastCtrl:ToastController,
    public loadingCtrl: LoadingController) {
    this.lote = navParams.data.lote;
    this.detail = navParams.data.detail;
    this.detail.fecha = new Date().toISOString();
    console.log(this.detail.fecha);    
    Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
      this.user_id = values[0];
      this.permiso = values[1];
    });
  }

  updateLote(){
    this.showLoader('Actualizando Lote, por favor espere!');
    this.restProvider.updateLote(this.user_id,this.permiso,this.lote,this.detail).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          if(res['result']!=0){
            let toast = this.toastCtrl.create({
    	        message: "Se ha actualizado el lote correctamente.",
    	        duration: 3000,
    	        position: 'bottom'
    	      });
    	      toast.present();
            this.navCtrl.pop();
          }else{
            let toast = this.toastCtrl.create({
    	        message: "Ocurrio un error al actualizar la información, intentelo nuevamente.",
    	        duration: 3000,
    	        position: 'bottom'
    	      });
    	      toast.present();
          }
        }
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoteModifyPage');
  }

  private showLoadingHandler(message) {
    if (this.loader == null) {
        this.loader = this.loadingCtrl.create({
            content: message
        });
        this.loader.present();
    } else {
        this.loader.data.content = message;
    }
  }

  private hideLoadingHandler() {
    if (this.loader != null) {
        this.loader.dismiss();
        this.loader = null;
    }
  }

  public showLoader(message) {
     this.showLoadingHandler(message);
  }

  public hideLoader() {
     this.hideLoadingHandler();
  }

}
