import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { OrderRowPage } from '../order-row/order-row';
import { OrderRow2Page } from '../order-row2/order-row2';


@IonicPage()
@Component({
  selector: 'page-order-list',
  templateUrl: 'order-list.html',
})
export class OrderListPage {

  order:any[];
  usuario:any;
  detalles:any[];
  success:any;
  user_id:any;
  permiso:any;
  identificador:any;
  orderRow:any[];

  descending: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage:Storage, public restProvider:RestProvider) {
      this.order = navParams.data.order;
      this.usuario = navParams.data.usuario;
      this.identificador = navParams.data.order.identificador;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderListPage');
  }

  ionViewWillEnter(){
    Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
      this.user_id = values[0];
      this.permiso = values[1];
      this.loadDetails();
    });
  }

  sort(){
    this.detalles.sort(function(a,b) {
      if ( a.codigo < b.codigo )
        return -1;
      if ( a.codigo > b.codigo )
        return 1;
      return 0;
    } );
    if(!this.descending){
      this.descending = true;
      this.detalles.reverse();
    }else{
      this.descending = false;
    }

  }

  sortUbicacion(){
    this.detalles.sort(function(a,b) {
      if ( a.ubicacion < b.ubicacion )
        return -1;
      if ( a.ubicacion > b.ubicacion )
        return 1;
      return 0;
    } );
    if(!this.descending){
      this.descending = true;
      this.detalles.reverse();
    }else{
      this.descending = false;
    }

  }

  loadDetails(){
    this.restProvider.loadOrderDetalles(this.user_id,this.permiso,this.identificador,this.usuario).subscribe(
      (res) => {
        console.log("Detalles");
        console.log(res);
        this.success = res['auth'];
        this.detalles = res['detalles'];
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  loadOrderItem(detalle){
    console.log("detalle");
    console.log(detalle);
    this.restProvider.loadOrderItem(this.user_id,this.permiso,detalle.identificador,detalle.codigo).subscribe(
      (res) => {
        this.success = res['auth'];

        if(this.success=="yes"){
          this.orderRow = res['order'];
          if(this.usuario=="usuario1")
            this.navCtrl.push(OrderRowPage,{orderRow:this.orderRow, detalle: detalle,usuario: this.usuario});
          if(this.usuario=="usuario2")
            this.navCtrl.push(OrderRow2Page,{orderRow:this.orderRow, detalle: detalle,usuario: this.usuario});
        }
      },
      (error) =>{
        console.error(error);
      }
    )
  }

}
