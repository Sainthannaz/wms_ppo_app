import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchAlmacenPage } from './search-almacen';

@NgModule({
  declarations: [
    SearchAlmacenPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchAlmacenPage),
  ],
})
export class SearchAlmacenPageModule {}
