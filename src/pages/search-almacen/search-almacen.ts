import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';
import { UbicacionDetailPage } from '../ubicacion-detail/ubicacion-detail';
import { UbicacionModifyPage } from '../ubicacion-modify/ubicacion-modify';
/**
 * Generated class for the SearchAlmacenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-almacen',
  templateUrl: 'search-almacen.html',
})
export class SearchAlmacenPage {
user_id:any;
  permiso:any;
  almacen: any;
  success:any;
  exists:any;
  almacenes:any[];
  ubicacion = {almacen:'',ubicacion:''}
  detail = {
    pasillo:'',
    modulo:'',
    nivel:'',
    alto:'',
    ancho:'',
    fondo:'',
    zona:'',
    picking:0,
    montacargas:0,
    piso:0,
    picking_val:'',
    montacargas_val:'',
    piso_val:'',
    seccion:''
  }
  ubicaciones:any[];
  
  loader: any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider:RestProvider, private storage:Storage, private toastCtrl:ToastController, 
    private alertCtrl:AlertController, public loadingCtrl: LoadingController) {
      Promise.all([this.storage.get("id"), this.storage.get("permiso"), this.storage.get("almacen")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
        this.loadAlmacenes();
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchAlmacenPage');
  }

  loadAlmacenes(){
    this.restProvider.loadAlmacenes(this.user_id,this.permiso).subscribe(
      (res) => {
        console.log("Result del provider");
        console.log(res);
        this.success = res['auth'];
        this.almacenes = res['almacenes'];
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  buscarUbicaciones(almacen){
  	console.log("Solicitamos ubicaciones...");
  	console.log(almacen);
  	this.showLoader('Cargando Ubicación!');
    this.restProvider.getUbicacionAlmacen(this.user_id,this.permiso, almacen).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
        	this.ubicaciones = res['result'];
        	console.log(this.ubicaciones);          
        }else{
          let toast = this.toastCtrl.create({
            message: "Ha ocurrido un error, intentelo nuevamente.",
            duration: 5000,
            position: 'bottom'
          });
          toast.present();
        }
        //this.total = res['total'];
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  modificar(ubicacion){
    console.log(ubicacion);
     this.showLoader('Cargando Ubicación!');
    this.restProvider.getUbicacion(this.user_id,this.permiso,ubicacion.almacen,ubicacion.ubicacion).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
         this.detail.pasillo = res['result'][0].pasillo;
          this.detail.modulo = res['result'][0].modulo;
          this.detail.nivel = res['result'][0].nivel;
          this.detail.seccion = res['result'][0].seccion;
          this.detail.alto = res['result'][0].alto;
          this.detail.ancho = res['result'][0].ancho;
          this.detail.fondo = res['result'][0].fondo;
          this.detail.zona = res['result'][0].zona;
          this.detail.picking = parseInt(res['result'][0].picking);
          this.detail.picking_val = res['result'][0].picking;
          this.detail.montacargas = parseInt(res['result'][0].montacargas);
          this.detail.montacargas_val = res['result'][0].montacargas;
          this.detail.piso = parseInt(res['result'][0].piso);
          this.detail.piso_val = res['result'][0].piso;
          this.navCtrl.push(UbicacionModifyPage,{detail:this.detail,ubicacion:this.ubicacion});
        }else{
          let toast = this.toastCtrl.create({
            message: "Ha ocurrido un error, intentelo nuevamente.",
            duration: 5000,
            position: 'bottom'
          });
          toast.present();
        }
        //this.total = res['total'];
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  eliminar(ubicacion){
    this.showLoader('Borrando Ubicación!');
    this.restProvider.deleteUbicacion(this.user_id,this.permiso, ubicacion.almacen, ubicacion.ubicacion).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){          
          let toast = this.toastCtrl.create({
            message: "Se ha eliminado la ubicación correctamente.",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
          this.buscarUbicaciones(ubicacion.almacen);
        }else{          
          let toast = this.toastCtrl.create({
            message: "Ha ocurrido un error al eliminar, intentelo nuevamente.",
            duration: 5000,
            position: 'bottom'
          });
          toast.present();
        }
        //this.total = res['total'];
      },
      (error) =>{
        console.error(error);
        this.hideLoader();
      }
    )
  }

  private showLoadingHandler(message) {
      if (this.loader == null) {
          this.loader = this.loadingCtrl.create({
              content: message
          });
          this.loader.present();
      } else {
          this.loader.data.content = message;
      }
  }

  private hideLoadingHandler() {
      if (this.loader != null) {
          this.loader.dismiss();
          this.loader = null;
      }
  }

  public showLoader(message) {
      this.showLoadingHandler(message);
  }

  public hideLoader() {
      this.hideLoadingHandler();
  }
}
