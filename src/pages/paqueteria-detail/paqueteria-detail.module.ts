import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaqueteriaDetailPage } from './paqueteria-detail';

@NgModule({
  declarations: [
    PaqueteriaDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(PaqueteriaDetailPage),
  ],
})
export class PaqueteriaDetailPageModule {}
