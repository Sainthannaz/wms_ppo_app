import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-paqueteria-detail',
  templateUrl: 'paqueteria-detail.html',
})
export class PaqueteriaDetailPage {

  user_id:any;
  permiso:any;
  sucursal:any;
  success:any;
  paqueteria = {
    paqueteria:'',
    direccion:'',
    telefono:'',
    contacto:'',
    celular:'',
    correo:''
  }
  loader: any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider:RestProvider, private storage:Storage, public toastCtrl:ToastController, public loadingCtrl: LoadingController) {
      this.sucursal = navParams.data.sucursal;
      Promise.all([this.storage.get("id"), this.storage.get("permiso")]).then(values => {
        this.user_id = values[0];
        this.permiso = values[1];
      });
  }

  toUpperCaseNombre(){
    console.log("Entro a upperCase Nombre");
    this.paqueteria.paqueteria = this.paqueteria.paqueteria.toUpperCase();
  }

  toUpperCaseDireccion(){
    console.log("Entro a upperCase Direccion");
    this.paqueteria.direccion = this.paqueteria.direccion.toUpperCase();
  }

  toUpperCaseContacto(){
    this.paqueteria.contacto = this.paqueteria.contacto.toUpperCase();
  }

  toUpperCaseTelefono(){
    this.paqueteria.telefono = this.paqueteria.telefono.toUpperCase();
  }

  toUpperCaseCelular(){
    this.paqueteria.celular = this.paqueteria.celular.toUpperCase();
  }

  toUpperCaseCorreo(){
    this.paqueteria.correo = this.paqueteria.correo.toUpperCase();
  }

  savePaqueteria(){
    this.showLoader('Agregando paqueteria, por favor espere!');
    this.restProvider.insertPaqueteria(this.user_id,this.permiso,this.sucursal,this.paqueteria).subscribe(
      (res) => {
        console.log(res);
        this.success = res['auth'];
        this.hideLoader();
        if(this.success=="yes"){
          let toast = this.toastCtrl.create({
            message: "Se ha agregado la paquetería correctamente.",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
          this.navCtrl.pop();
        }else{

          let toast = this.toastCtrl.create({
            message: "Ha ocurrido un error al guardar la información, intentelo nuevamente.",
            duration: 5000,
            position: 'bottom'
          });
          toast.present();
        }
      },
      (error) =>{
        this.hideLoader();
        console.error(error);
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaqueteriaDetailPage');
  }

  private showLoadingHandler(message) {
    if (this.loader == null) {
        this.loader = this.loadingCtrl.create({
            content: message
        });
        this.loader.present();
    } else {
        this.loader.data.content = message;
    }
  }

  private hideLoadingHandler() {
    if (this.loader != null) {
        this.loader.dismiss();
        this.loader = null;
    }
  }

  public showLoader(message) {
     this.showLoadingHandler(message);
  }

  public hideLoader() {
     this.hideLoadingHandler();
  }
}
