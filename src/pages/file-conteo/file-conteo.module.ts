import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FileConteoPage } from './file-conteo';

@NgModule({
  declarations: [
    FileConteoPage,
  ],
  imports: [
    IonicPageModule.forChild(FileConteoPage),
  ],
})
export class FileConteoPageModule {}
