import { Component } from '@angular/core';
import { IonicPage, MenuController, NavController, Platform, ToastController, LoadingController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { User } from '../../providers/user/user';
import { DashboardPage } from '../pages';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	account: { usuario: string, password: string } = {
    	usuario: '',
    	password: ''
  	};

  	userProfile: any;
  	private loginErrorString: string;
  	private errorString: string;
  	public type = 'password';
  	public showPass = false;
  	public unregisterBackButtonAction: any;
	constructor(public navCtrl: NavController,  public platform: Platform, public loadingCtrl: LoadingController,
		public translateService: TranslateService, public menu: MenuController, private sqlite: SQLite,
    public user: User, public toastCtrl:ToastController, private storage: Storage) {
		this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      	this.loginErrorString = value;
    	})
	}

	doLogin(){
	  	let loader = this.loadingCtrl.create({
      		spinner: 'dots',      
      	});  
      	loader.present();
      	console.log(this.account);
		this.user.login(this.account).subscribe((resp) => {
	      if (resp['auth'] == "yes"){
	      	loader.dismiss();
	        this.navCtrl.push(DashboardPage);
	      } else if (resp['auth'] == "no"){
	      	loader.dismiss();
	      }
	    }, (err) => {
	      // Unable to log in
	      loader.dismiss();
	      let toast = this.toastCtrl.create({
	        message: this.loginErrorString,
	        duration: 3000,
	        position: 'bottom'
	      });
	      toast.present();
	    });
	   
	}

	showPassword() {
	    this.showPass = !this.showPass;
	    if(this.showPass){
	      this.type = 'text';
	    } else {
	      this.type = 'password';
	    }
	}

	ionViewDidLoad(){
		this.storage.get('loginSession').then((val) => {
          console.log('Login state now is: ', val);
          if (val == "true"){
            console.log("true");
            this.navCtrl.push(DashboardPage);
            //this.rootPage = 'TabsPage';
          }
          if (val == "false"){
            console.log("false");
          }
        });
	}


	ionViewDidEnter() {
		// the root left menu should be disabled on the tutorial page
		this.menu.enable(false);
	}

  	ionViewWillLeave() {
    	// enable the root left menu when leaving the tutorial page
    	this.menu.enable(true);
    	this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  	}

  	initializeBackButtonCustomHandler(): void {
	    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
	      console.log('Prevent Back Button Page Change');
	    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
	}

	
}
